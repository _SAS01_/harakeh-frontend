import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class BusModel
{
  int id;
  String startingStation;
  String route;
  List<dynamic> workingDays;
  List<String> workingDaysStrings;
  int stHour,enHour;
  bool free;
  List<String> BusesList = ["1","2","3","4","5","6","7","8"];

  BusModel(
      {
        required this.id,
        required this.startingStation,
        required this.route,
        required this.workingDays,
        required this.stHour,
        required this.enHour,
        required this.free,
        required this.workingDaysStrings

      }
      );

}
class BusAPI extends ChangeNotifier{
  List<BusModel> BusesList = [];
  List<BusModel> availbleBuses = [];
  Future getBuses() async {
    String ip = 'https://harakeh-backend.herokuapp.com/api';
    Uri url = Uri.parse('$ip/buses');
    http.Response response = await http.get(
      url,
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'X-ACCESS-TOKEN':
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmQxYzAxNjliY2I3NThlZmUyY2Y1ZmEiLCJpc0FkbWluIjp0cnVlLCJpYXQiOjE2NTc5MjAzNzh9.mwGPvdRkCq-H_ivKR1oE-A-RIQNFZBT4GgiGWBbK2yQ',
      },
    );
    print('dsass');
    final body = jsonDecode(response.body) as List<dynamic>;
    final List<BusModel> temp = [];
    for (int i = 0; i < body.length; i++) {
      temp.add(
        BusModel(id: body[i]['id'], startingStation: body[i]['startingStation']['name'], route: body[i]['route'], workingDays: body[i]['workingDays'], stHour: body[i]['stHour'], enHour: body[i]['enHour'], free: body[i]['free'] , workingDaysStrings: ['Sun']));
          }
          BusesList = temp;
          print(BusesList.length);
      for (int i = 0; i < BusesList.length; i++) {
        print('${BusesList[i].id}');
      }
      print('${body}');
      notifyListeners();

    }
  Future getFreeBuses( {required String token}
      ) async {
    String ip = 'https://harakeh-backend.herokuapp.com/api';
    Uri url = Uri.parse('$ip/buses/free');
    http.Response response = await http.get(
      url,
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'X-ACCESS-TOKEN':
token      },
    );
    print('dsass');
    final body = jsonDecode(response.body) as List<dynamic>;
    final List<BusModel> temp = [];
    for (int i = 0; i < body.length; i++) {
      temp.add(
          BusModel(id: body[i]['id'], startingStation: body[i]['startingStation']['name'], route: body[i]['route'], workingDays: body[i]['workingDays'], stHour: body[i]['stHour'], enHour: body[i]['enHour'], free: body[i]['free'] , workingDaysStrings: []));
    }
    availbleBuses = temp;
    print(availbleBuses.length);

    print('${body}');
    notifyListeners();
    int x = response.statusCode ;
    return x;
  }

}