import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
class User extends ChangeNotifier{
  int? userTd;
  String? userName;
  String? userEmail;
  String? userPassword;
  String? nationalId;


  User({required this.userName , required this.userEmail , required this.userPassword , required this.nationalId});

  void addUser(String _username , String _userEmail , String _userPassowrd , String _nationalId){
    userName = _username ;
    userEmail = _userEmail;
    userPassword = _userPassowrd;
    nationalId = _nationalId;
    notifyListeners();
  }

}
class UserProvider extends ChangeNotifier {
}