import 'dart:convert';

import 'package:admin/Screens/Trips/trips.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Trip extends ChangeNotifier{
  String name;
  int numBuses;
  List<dynamic>stations;
  int numStations;
  List<dynamic> buses;
  List<dynamic> time_stations;

  Trip (
      { required this.name,
        required this.time_stations,
        required this.buses,
        required this.numBuses,
        required this.numStations,
        required this.stations
      });


}
class TripAPI extends ChangeNotifier{

  List<Trip> Trips = [
    Trip(name: 'trip number 1', time_stations: [12,20,30], buses: ["1","2","3"], numBuses: 3, numStations: 3, stations: ["a","b","c"]),
    Trip(name: 'trip number 2', time_stations: [12,20,30], buses: ["1","2","3"], numBuses: 3, numStations: 3, stations: ["a","b","c"]),
    Trip(name: 'trip number 3', time_stations: [12,20,30], buses: ["1","2","3"], numBuses: 3, numStations: 3, stations: ["a","b","c"])

  ];

  void addStation(int index){
    Trips[index].stations.add('Choose Station');
    Trips[index].time_stations.add(0);
    Trips[index].numStations++;
    notifyListeners();
  }
  void addBus(int index){
    Trips[index].buses.add('0');
    Trips[index].numBuses++;
    notifyListeners();
  }
  void deleteStation(int indexTop , int index)
  {
      Trips[indexTop].stations.removeAt(index);
      Trips[indexTop].time_stations.removeAt(index);
      Trips[index].numStations--;
      notifyListeners();
  }
  void deleteBus(int indexTop ,int index)
  {
    Trips[indexTop].buses.removeAt(index);
    Trips[index].numBuses--;
    notifyListeners();
  }
  Future getTrips(    {required String token}
      ) async {
    String ip = 'https://harakeh-backend.herokuapp.com/api';
    Uri url = Uri.parse('$ip/routes');
    print('dsada');
    http.Response response = await http.get(
      url,
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'X-ACCESS-TOKEN':
token      },
    );
    print('1');
    final body = jsonDecode(response.body) as List<dynamic>;
    print('2');

    final List<Trip> temp = [];
    print('3');
   // print(body[0]['time']);

    for (int i = 0; i < body.length; i++) {
      List<dynamic> times = body[i]['time'];
      List<dynamic> buses = body[i]['BusesId'];
      List<String> stations = [];

      for(int j=0;j<body[i]['numStations'];j++)
        {
          stations.add(body[i]['stations'][j]['name']);
        }

      temp.add(
        Trip(name: body[i]['name'], time_stations: times, buses: buses, numBuses: body[i]['numBuses'], numStations: body[i]['numStations'], stations: stations));
    }
    print('4');
    Trips = temp;
    print(Trips.length);
    for (int i = 0; i < Trips.length; i++) {
      print('${Trips[i].name}');
    }
    print('${body}');
    notifyListeners();
    return response.statusCode;

  }

}