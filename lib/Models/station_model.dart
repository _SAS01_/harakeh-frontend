import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;

class Station extends ChangeNotifier {
  LatLng MainLocation;
  String name;
  double lat;
  double lng;

  Station({
    required this.lat,
    required this.lng,
    required this.name,
    required this.MainLocation
  });
  void setlongla(lat, long) {
    MainLocation = LatLng(lat, long);
    notifyListeners();
  }

  final Set<Marker> markers = new Set();
  Set<Marker> myMarker() {
    markers.add(Marker(
      markerId: MarkerId(MainLocation.toString()),
      position: MainLocation,
      icon: BitmapDescriptor.defaultMarker,
    ));
    notifyListeners();
    return markers;
  }

  get getmain => MainLocation;
}

class StationModel extends ChangeNotifier {
  String ip = 'https://harakeh-backend.herokuapp.com/api';
  List<Station> StationList = [];

  Future getStations(    {required String token}
      ) async {
    print('dsa');
    Uri url = Uri.parse('$ip/stations');
    print('dsada');
    http.Response response = await http.get(
      url,
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'X-ACCESS-TOKEN':
token      },
    );
    print('dsass');
    final body = jsonDecode(response.body) as List<dynamic>;
    final List<Station> temp = [];
    for (int i = 0; i < body.length; i++) {
      temp.add(
        Station(
            lat: body[i]["lat"], lng: body[i]["lng"], name: body[i]["name"] ,MainLocation: LatLng(body[i]["lat"],body[i]["lng"])),
      );
    }
    StationList = temp;
    print(StationList.length);
    for (int i = 0; i < StationList.length; i++) {
      print('${StationList[i].name}');
    }
    print('${body}');
    notifyListeners();
    return response.statusCode;

  }

  LatLng mainLocation = LatLng(33.5138, 36.2765);

  get getList => StationList;

  void Setlonglat(lat, long,Station station ) {
    station.setlongla(lat, long);
    notifyListeners();
  }

  final Set<Marker> markers = new Set();
  Set<Marker> myMarker() {
    markers.add(Marker(
      markerId: MarkerId(mainLocation.toString()),
      position: mainLocation,
      icon: BitmapDescriptor.defaultMarker,
    ));
    notifyListeners();
    return markers;
  }
}
