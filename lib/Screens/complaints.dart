import 'package:admin/Models/complaints_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import '../Components/constants.dart';
import '../Components/search_bar.dart';


class Complaints extends StatefulWidget {
  static String id = 'newsScreen';

  @override
  State<Complaints> createState() => ComplaintsState();
}

class ComplaintsState extends State<Complaints> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Complaints',
          style: GoogleFonts.montserrat(fontWeight: FontWeight.w500),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: NewsBody(),
    );
  }
}

class NewsBody extends StatefulWidget {
  @override
  State<NewsBody> createState() => _NewsBodyState();
}

class _NewsBodyState extends State<NewsBody> {
  late List<dynamic> complaints = [];
  String query = '';

  @override
  void initState() {
    super.initState();

    complaints = Provider.of<ComplaintsAPI>(context, listen: false).complaintsList;
  }

  @override
  Widget build(BuildContext context1) {
    return Column(
      children: [
        Stack(
          children: [
            Container(
              margin: EdgeInsets.only(
                  bottom: MediaQuery.of(context).size.height / 22),
              height: MediaQuery.of(context).size.height / 6,
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25)),
                  gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      stops: [
                        0.02,
                        0.5,
                      ],
                      colors: [
                        Color(0xFF2c8e82),
                        Color(0xFF42dc8f),
                      ])),
            ),
            Positioned(
              child: SearchBar(),
              bottom: 0,
              left: 0,
              right: 0,
            ),
          ],
        ),
        SizedBox(
          height: 15,
        ),
        Expanded(
          //Listview
          child: MediaQuery.removePadding(
            context: context,
            removeTop: true,
            child: ListView.builder(
              //shrinkWrap: true,
              itemCount: complaints.length,
              itemBuilder: (context, index) {
                final complaints1 = complaints[index];
                return buildNews(complaints1, context1, index);
              },
            ),
          ),
        ),
      ],
    );
  }

  Widget SearchBar() => SearchWidget(
    text: query,
    hintText: 'Search for a news..',
    onChanged: searchNews,
  );

  void searchNews(String query) {
    final complaints =
    Provider.of<ComplaintsAPI>(context, listen: false).complaintsList.where((complaint) {
      final nameLower = complaint.route.toLowerCase();
      final searchLower = query.toLowerCase();

      return nameLower.contains(searchLower);
    }).toList();

    setState(() {
      this.query = query;
      this.complaints = complaints;
    });
  }

  Widget buildNews(ComplaintsModel complaints1, BuildContext context, int index) {
    return ListTile(
        leading: SvgPicture.asset(
          'assets/complaint.svg',
          color: mainColor,
          fit: BoxFit.cover,
          width: 50,
          height: 50,
        ),
        title: Text('${complaints1.content}',
style: GoogleFonts.montserrat(fontWeight: FontWeight.w500),
        ),
        subtitle: Text('${complaints1.route}, bus number is ${complaints1.busId}'),
        onTap: () {

        });
  }
}
