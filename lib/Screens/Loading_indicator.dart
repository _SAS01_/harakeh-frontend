import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:loading_indicator/loading_indicator.dart';

class LoadingScreenIndicator extends StatelessWidget {
  const LoadingScreenIndicator({Key? key}) : super(key: key);
  static String id = 'Loading';

  @override
  Widget build(BuildContext context) {
    double MediaWidth = MediaQuery.of(context).size.width;
    double MediaHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          children: [
            SizedBox(
              height: 200,
            ),
            Stack(
              children: [
                Padding(
                  padding: EdgeInsets.only(
                      left: MediaWidth * 0.3,
                      right: MediaWidth * 0.3,
                      top: MediaHeight * 0.18),
                  child: Container(
                    child: LoadingIndicator(
                      indicatorType: Indicator.circleStrokeSpin,

                      /// Required, The loading type of the widget
                      colors: const [Color(0xFF32bd8d)],

                      /// Optional, The color collections
                      strokeWidth: 7,

                      /// Optional, The stroke of the line, only applicable to widget which contains line
                      backgroundColor: Colors.white,

                      /// Optional, Background of the widget
                      pathBackgroundColor: Colors.white,

                      /// Optional, the stroke backgroundColor
                    ),
                  ),
                ),
                Center(
                  child: Padding(
                    padding: EdgeInsets.only(top: MediaHeight * 0.23),
                    child: Container(
                      margin: EdgeInsets.all(20),
                      child: Text(
                        'Loading...',
                        style: GoogleFonts.montserrat(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
