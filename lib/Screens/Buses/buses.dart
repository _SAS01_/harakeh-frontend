import 'package:admin/Screens/Buses/bus_details.dart';
import 'package:admin/service/signin_api.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import '../../Components/search_bar.dart';
import 'package:admin/Models/bus_model.dart';

late List<BusModel> temp =[];

class Buses extends StatefulWidget {
  static String id = 'buses';

  @override
  State<Buses> createState() => BusesState();
}

class BusesState extends State<Buses> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Buses',
          style: GoogleFonts.montserrat(fontWeight: FontWeight.w500),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
      )

      ,
      body: BusesBody(),
    );
  }
}

class BusesBody extends StatefulWidget {
  @override
  State<BusesBody> createState() => _BusesBodyState();
}

class _BusesBodyState extends State<BusesBody> {
  late List<BusModel> buses =[] ;
  late List<BusModel> Buses=[];
  String query = '';

  @override
  void initState() {
    super.initState();
    temp = Provider.of<BusAPI>(context, listen: false).BusesList;
    print(temp.length);
    for(int i=0;i<temp.length;i++)
      {
        Buses.add(temp[i]);
      }
  }

  @override
  Widget build(BuildContext context1) {
    return Column(
      children: [
        Stack(
          children: [
            Container(
              margin: EdgeInsets.only(
                  bottom: MediaQuery.of(context).size.height / 22),
              height: MediaQuery.of(context).size.height / 6,
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25)),
                  gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      stops: [
                        0.02,
                        0.5,
                      ],
                      colors: [
                        Color(0xFF2c8e82),
                        Color(0xFF42dc8f),
                      ])),
            ),
            Positioned(
              child: SearchBar(),
              bottom: 0,
              left: 0,
              right: 0,
            ),
          ],
        ),
        SizedBox(
          height: 15,
        ),
        Expanded(
          child: MediaQuery.removePadding(
            context: context,
            removeTop: true,
            child: ListView.builder(
              //shrinkWrap: true,
              itemCount: Buses.length,
              itemBuilder: (context, index) {
                final bus = Buses[index].id;
                return buildBus(bus, context1, index);
              },
            ),
          ),
        ),
      ],
    );
  }

  Widget SearchBar() => SearchWidget(
        text: query,
        hintText: 'Search for a Bus..',
        onChanged: searchBus,
      );

  void searchBus(String query) {
    final buses =
        Provider.of<BusAPI>(context, listen: false).BusesList.where((bus) {
          final nameLower = bus.id.toString().toLowerCase();
          final searchLower = query.toString().toLowerCase();
          return nameLower.contains(searchLower);
    }).toList();

    setState(() {
      this.query = query;
      this.Buses = buses;
    });
  }

  Widget buildBus(int bus, BuildContext context, int index) {
    return ListTile(
      leading: Image.asset(
        'assets/bus.png',
        fit: BoxFit.cover,
        width: 40,
        height: 40,
      ),
      title: Text(
        '${Buses[index].id}',
        //BusModel().BusesList[index],
        style: GoogleFonts.montserrat(fontWeight: FontWeight.w500),
      ),
      subtitle: Text('Click to edit'),
        onTap: ()  {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return BusDetails(
              index: index,
            );
          }));
        }
    );
  }
}
