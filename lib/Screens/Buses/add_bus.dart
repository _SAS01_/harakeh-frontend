import 'package:admin/Components/constants.dart';
import 'package:admin/Models/station_model.dart';
import 'package:admin/Models/trips_model.dart';
import 'package:admin/service/http.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:new_gradient_app_bar/new_gradient_app_bar.dart';
import 'package:provider/provider.dart';
import 'package:day_picker/day_picker.dart';
import 'package:admin/Models/bus_model.dart';
import 'package:flutter_switch/flutter_switch.dart';

import '../../service/signin_api.dart';
import '../HomePage/newwelcom.dart';

late bool free = false;
TimeOfDay time = TimeOfDay.now();
TimeOfDay timeEnd = TimeOfDay.now();
BusModel busObject = new BusModel(
    id: 0,
    startingStation: '',
    route: '',
    workingDays: [],
    stHour: 0,
    enHour: 0,
    free: false,
    workingDaysStrings: []);

class add_bus extends StatefulWidget {
  static String id = 'add_bus';
  static final _formKey5 = GlobalKey<FormState>();

  @override
  State<add_bus> createState() => _add_busState();
}

class _add_busState extends State<add_bus> {
  @override
  Widget build(BuildContext context) {
    var MediaHeight = MediaQuery.of(context).size.height;
    var MediaWidth = MediaQuery.of(context).size.width;
    String selecteditem = "Starting Station";
    String selecteditem2 = "Starting Route";
    late String startingStation = '', route = '';
    late List<String> workingDays = [];
    late int stHour = 0, enHour = 0, id = 0;
    List<String> places = [];
    List<String> trips = [];
    Http http = new Http();
    List<DayInWeek> _days = [
      DayInWeek(
        "Sun",
      ),
      DayInWeek(
        "Mon",
      ),
      DayInWeek(
        "Tue",
      ),
      DayInWeek(
        "Wed",
      ),
      DayInWeek(
        "Thu",
      ),
      DayInWeek(
        "Fri",
      ),
      DayInWeek(
        "Sat",
      ),
    ];
    void showError(String message) {
      showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text('Something went wrong'),
            content: Text(message),
            actions: [
              MaterialButton(
                  child: Text(
                    'Okay',
                    style: TextStyle(color: Colors.white),
                  ),
                  color: mainColor,
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pop();
                  }),
            ],
          ));
    }

    void showSuccess(String message) {
      showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text('Great Work!'),
            content: Text(message),
            actions: [
              MaterialButton(
                  child: Text(
                    'Okay',
                    style: TextStyle(color: Colors.white),
                  ),
                  color: mainColor,
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pushReplacement(MaterialPageRoute(builder: (context)=>Welcome()));

                  }),
            ],
          ));
    }

    return ChangeNotifierProvider(create: (context) {
      return BusAPI();
    }, child: Consumer<BusAPI>(
      builder: (context, bus, child) {
        return Scaffold(
          appBar:  NewGradientAppBar(
              leading: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(Icons.keyboard_arrow_left),
              ),
              centerTitle: true,
              //backgroundColor: Color(0xFF00D3A0),
              elevation: 0,
              title: Text(
                'Add Bus',
                style: GoogleFonts.montserrat(fontWeight: FontWeight.w500),
              ),
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                    Color(0xFF2c8e82),
                    Color(0xFF42dc8f),
                  ])),
          floatingActionButton: FloatingActionButton(
            onPressed: () async {
              if (add_bus._formKey5.currentState!.validate()) {
                var x = await http.add_bus(
                    id: busObject.id,
                    startingStation: busObject.startingStation,
                    route: busObject.route,
                    workingDays: busObject.workingDaysStrings,
                    stHour: busObject.stHour,
                    enHour: busObject.enHour,
                    free: busObject.free,
                    token: Provider.of<SignInProvider>(context, listen: false)
                        .token);
                if (x == 'Done Successfully') {
                  showSuccess(x);
                } else {
                  showError(x);
                }
              }
            },
            child: Icon(Icons.check),
            backgroundColor: Color(0xFF32bd8d),
          ),
          body: SingleChildScrollView(
            child: Form(
              key: add_bus._formKey5,
              child: Padding(
                padding: EdgeInsets.only(
                    top: MediaHeight * 0.1,
                    left: MediaWidth * 0.07,
                    right: MediaWidth * 0.07),
                child: Center(
                  child: Column(
                    children: [
                      TextFormField(
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20)),
                              borderSide: BorderSide(
                                  width: 2, color: Color(0xff33C58E))),
                          labelText: 'Bus Id',
                          labelStyle: TextStyle(color: Colors.black),
                          prefixIcon: Icon(
                            Icons.directions_bus,
                            color: Color(0xff33C58E),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                          ),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "Requird Field";
                          } else
                            return null;
                        },
                        onChanged: (value) {
                          id = int.parse(value);
                          busObject.id = id;
                        },
                        keyboardType: TextInputType.number,
                      ),
                      SizedBox(
                        height: MediaHeight * 0.03,
                      ),
                      Consumer<StationModel>(
                          builder: (context, station, child) {
                        for (int i = 0; i < station.StationList.length; i++) {
                          places.add(station.StationList[i].name);
                        }
                        return Container(
                          height: MediaHeight * 0.06,
                          child: DropdownSearch<String>(
                            items: places,
                            popupProps: PopupPropsMultiSelection.dialog(
                              showSelectedItems: true,
                              showSearchBox: true,
                            ),
                            validator: (value) => value == "Starting Station"
                                ? 'field required'
                                : null,
                            onChanged: (value) {
                              selecteditem = value!;
                              for (int i = 0;
                                  i < station.StationList.length;
                                  i++) {
                                if (value == station.StationList[i].name) {
                                  startingStation = station.StationList[i].name;
                                  busObject.startingStation = startingStation;
                                  break;
                                }
                              }
                            },
                            selectedItem: selecteditem,
                          ),
                        );
                      }),
                      SizedBox(
                        height: MediaHeight * 0.03,
                      ),
                      Consumer<TripAPI>(builder: (context, trip, child) {
                        for (int i = 0; i < trip.Trips.length; i++) {
                          trips.add(trip.Trips[i].name);
                        }
                        return Container(
                          height: MediaHeight * 0.06,
                          child: DropdownSearch<String>(
                            items: trips,
                            popupProps: PopupPropsMultiSelection.dialog(
                              showSelectedItems: true,
                              showSearchBox: true,
                            ),
                            validator: (value) => value == "Starting Route"
                                ? 'field required'
                                : null,
                            onChanged: (value) {
                              selecteditem = value!;
                              for (int i = 0; i < trip.Trips.length; i++) {
                                if (value == trip.Trips[i].name) {
                                  route = trip.Trips[i].name;
                                  busObject.route = route;
                                  break;
                                }
                              }
                              print(route);
                            },
                            selectedItem: selecteditem2,
                          ),
                        );
                      }),
                      SizedBox(
                        height: MediaHeight * 0.02,
                      ),
                      Container(
                        child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: SelectWeekDays(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            days: _days,
                            border: false,
                            boxDecoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30.0),
                              gradient: LinearGradient(
                                begin: Alignment.topLeft,
                                colors: [
                                  Color(0xFF2c8e82),
                                  Color(0xFF42dc8f),
                                ],
                                tileMode: TileMode
                                    .repeated, // repeats the gradient over the canvas
                              ),
                            ),
                            onSelect: (values) {
                              workingDays = values;
                              busObject.workingDaysStrings = workingDays;
                              print(workingDays);
                            },
                          ),
                        ),
                      ),
                      SizedBox(
                        height: MediaHeight * 0.02,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Column(
                            children: [
                              Container(
                                child: Text(
                                  '${time.hour}',
                                  style: TextStyle(fontSize: 20),
                                ),
                              ),
                              ElevatedButton(
                                  onPressed: () async {
                                    TimeOfDay? newTime = await showTimePicker(
                                        context: context,
                                        initialTime: time,
                                        builder: (context, child) {
                                          return MediaQuery(
                                              data: MediaQuery.of(context)
                                                  .copyWith(
                                                      alwaysUse24HourFormat:
                                                          true),
                                              child: child!);
                                        });
                                    if (newTime == null) return;

                                    setState(() {
                                      time = newTime;
                                      print(newTime);
                                    });

                                    busObject.stHour = stHour;
                                  },
                                  style: ElevatedButton.styleFrom(
                                      primary: Color(0xFF32bd8d)),
                                  child: Text('Start Hour')),
                            ],
                          ),
                          Column(
                            children: [
                              Container(
                                child: Text(
                                  '${timeEnd.hour}',
                                  style: TextStyle(fontSize: 20),
                                ),
                              ),
                              ElevatedButton(
                                  onPressed: () async {
                                    TimeOfDay? newTime = await showTimePicker(
                                        context: context,
                                        initialTime: timeEnd,
                                        builder: (context, child) {
                                          return MediaQuery(
                                              data: MediaQuery.of(context)
                                                  .copyWith(
                                                      alwaysUse24HourFormat:
                                                          true),
                                              child: child!);
                                        });
                                    if (newTime == null) return;

                                    setState(() {
                                      timeEnd = newTime;
                                      print(newTime);
                                    });

                                    busObject.enHour = enHour;
                                  },
                                  style: ElevatedButton.styleFrom(
                                      primary: Color(0xFF32bd8d)),
                                  child: Text('End Hour'))
                            ],
                          ),
                        ],
                      ),
                      SizedBox(height: MediaHeight * 0.03),
                      Container(
                        child: Text(
                          'Active Status',
                          style: TextStyle(fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        child: Center(
                          child: FlutterSwitch(
                            activeColor: Color(0xFF32bd8d),
                            width: 130,
                            height: 40.0,
                            valueFontSize: 20.0,
                            toggleSize: 30,
                            value: free,
                            activeText: 'Active',
                            borderRadius: 30.0,
                            padding: 8.0,
                            showOnOff: true,
                            onToggle: (val) {
                              setState(() {
                                free = val;
                                busObject.free = free;
                              });
                            },
                          ),
                        ),
                      ),
                      SizedBox(
                        height: MediaHeight * 0.2,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    ));
  }
}
