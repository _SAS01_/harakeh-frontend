import 'package:admin/Components/constants.dart';
import 'package:admin/Models/station_model.dart';
import 'package:admin/Models/trips_model.dart';
import 'package:admin/service/http.dart';
import 'package:day_picker/day_picker.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:admin/Models/bus_model.dart';

import '../../service/signin_api.dart';
import '../HomePage/newwelcom.dart';

late bool free;
late TimeOfDay time;
late TimeOfDay timeEnd;
late int currentId;
BusModel busObject = new BusModel(
    id: 0,
    startingStation: '',
    route: '',
    workingDays: [],
    stHour: 0,
    enHour: 0,
    free: false,
    workingDaysStrings: []);

class BusDetails extends StatefulWidget {
  final int index;
  static String id = 'bus_details';

  const BusDetails({required this.index});

  @override
  State<BusDetails> createState() => _BusDetailsState();
}

class _BusDetailsState extends State<BusDetails> {
  @override
  Widget build(BuildContext context) {
    void showError(String message) {
      showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text('Something went wrong'),
            content: Text(message),
            actions: [
              MaterialButton(
                  child: Text(
                    'Okay',
                    style: TextStyle(color: Colors.white),
                  ),
                  color: mainColor,
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pop();
                  }),
            ],
          ));
    }

    void showSuccess(String message) {
      showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text('Great Work!'),
            content: Text(message),
            actions: [
              MaterialButton(
                  child: Text(
                    'Okay',
                    style: TextStyle(color: Colors.white),
                  ),
                  color: mainColor,
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pushReplacement(MaterialPageRoute(builder: (context)=>Welcome()));

                  }),
            ],
          ));
    }

    Http http = new Http();

    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              stops: [
            0.1,
            0.28,
          ],
              colors: [
            Color(0xFF2c8e82),
            Color(0xFF42dc8f),
          ])),
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.transparent,
        // extendBodyBehindAppBar: true,
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            'Bus Details',
            style: GoogleFonts.montserrat(fontWeight: FontWeight.w500),
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
          actions: [
            IconButton(
                onPressed: () async {
                  var x = await http.deleteBus(
                      id: busObject.id,
                      token: Provider.of<SignInProvider>(context, listen: false)
                          .token);
                  if (x == 'Done Successfully') {
                    showSuccess(x);
                  } else {
                    showError(x);
                  }
                },
                icon: Icon(
                  Icons.delete,
                ))
          ],
        ),
        body: Column(
          children: [
            SizedBox(
              height: 25,
            ),
            Expanded(
              child: Stack(
                children: [
                  Container(
                      decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30)),
                  )),
                  SingleChildScrollView(child: BusDetailsBody(widget.index)),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class BusDetailsBody extends StatefulWidget {
  final int index;

  BusDetailsBody(this.index);
  @override
  State<BusDetailsBody> createState() => _BusDetailsBodyState(index);
}

class _BusDetailsBodyState extends State<BusDetailsBody> {
  final int index;

  _BusDetailsBodyState(this.index);
  @override
  List<String> bus = [];

  late List<BusModel> temp = [];
  void showError(String message) {
    showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: Text('Something went wrong'),
          content: Text(message),
          actions: [
            MaterialButton(
                child: Text(
                  'Okay',
                  style: TextStyle(color: Colors.white),
                ),
                color: mainColor,
                onPressed: () {
                  Navigator.of(context, rootNavigator: true).pop();
                }),
          ],
        ));
  }

  void showSuccess(String message) {
    showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: Text('Great Work!'),
          content: Text(message),
          actions: [
            MaterialButton(
                child: Text(
                  'Okay',
                  style: TextStyle(color: Colors.white),
                ),
                color: mainColor,
                onPressed: () {
                  Navigator.of(context, rootNavigator: true).pushReplacement(MaterialPageRoute(builder: (context)=>Welcome()));

                }),
          ],
        ));
  }

  void initState() {
    super.initState();

    temp = Provider.of<BusAPI>(context, listen: false).BusesList;


    print(index);
    for (int i = 0; i < temp.length; i++) {
      bus.add(temp[i].id.toString());
    }
    busObject.id = temp[index].id;
    busObject.startingStation = temp[index].startingStation;
    busObject.route = temp[index].route;
    busObject.workingDaysStrings = temp[index].workingDaysStrings;
    busObject.stHour = temp[index].stHour;
    busObject.enHour = temp[index].enHour;
    busObject.free = temp[index].free;
    time = TimeOfDay(hour: busObject.stHour, minute: 00);
    timeEnd = TimeOfDay(hour: busObject.enHour, minute: 00);
    free = busObject.free;

    currentId = temp[index].id;
    print(currentId);
  }

  @override
  Widget build(BuildContext context) {
    var MediaWidth = MediaQuery.of(context).size.width;
    var MediaHeight = MediaQuery.of(context).size.height;
    String selecteditem = "${busObject.startingStation}";
    String selecteditem2 = "${busObject.route}";
    List<String> places = [];
    List<String> trips = [];
    Http http = new Http();
    List<DayInWeek> _days = [
      DayInWeek(
        "Sun",
        isSelected: temp[index].workingDays[0],
      ),
      DayInWeek("Mon", isSelected: temp[index].workingDays[1]),
      DayInWeek("Tue", isSelected: temp[index].workingDays[2]),
      DayInWeek("Wed", isSelected: temp[index].workingDays[3]),
      DayInWeek("Thu", isSelected: temp[index].workingDays[4]),
      DayInWeek("Fri", isSelected: temp[index].workingDays[5]),
      DayInWeek("Sat", isSelected: temp[index].workingDays[6]),
    ];
    return Padding(
      padding: EdgeInsets.only(
          top: MediaHeight * 0.01,
          left: MediaWidth * 0.07,
          right: MediaWidth * 0.07),
      child: Column(
        children: [
          SizedBox(
            height: 25,
          ),
          TextField(
            decoration: InputDecoration(
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  borderSide: BorderSide(width: 2, color: Color(0xff33C58E))),
              hintText: 'Edit Bus ' + bus[index] + ' id',
              labelStyle: TextStyle(color: Colors.black),
              prefixIcon: Icon(
                Icons.account_circle,
                color: Color(0xff33C58E),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(20)),
              ),
            ),
            onChanged: (value) {
              busObject.id = int.parse(value);
            },
            keyboardType: TextInputType.number,
          ),
          SizedBox(
            height: MediaHeight * 0.03,
          ),
          Consumer<StationModel>(builder: (context, station, child) {
            for (int i = 0; i < station.StationList.length; i++) {
              places.add(station.StationList[i].name);
            }
            return Container(
              height: MediaHeight * 0.06,
              child: DropdownSearch<String>(
                items: places,
                popupProps: PopupPropsMultiSelection.dialog(
                  showSelectedItems: true,
                  showSearchBox: true,
                ),
                validator: (value) =>
                    value == "Starting Station" ? 'field required' : null,
                onChanged: (value) {
                  selecteditem = value!;
                  for (int i = 0; i < station.StationList.length; i++) {
                    if (value == station.StationList[i].name) {
                      busObject.startingStation = station.StationList[i].name;
                      break;
                    }
                  }
                },
                selectedItem: selecteditem,
              ),
            );
          }),
          SizedBox(
            height: MediaHeight * 0.03,
          ),
          Consumer<TripAPI>(builder: (context, trip, child) {
            for (int i = 0; i < trip.Trips.length; i++) {
              trips.add(trip.Trips[i].name);
            }
            return Container(
              height: MediaHeight * 0.06,
              child: DropdownSearch<String>(
                items: trips,
                popupProps: PopupPropsMultiSelection.dialog(
                  showSelectedItems: true,
                  showSearchBox: true,
                ),
                validator: (value) =>
                    value == "Starting Route" ? 'field required' : null,
                onChanged: (value) {
                  selecteditem = value!;
                  for (int i = 0; i < trip.Trips.length; i++) {
                    if (value == trip.Trips[i].name) {
                      busObject.route = trip.Trips[i].name;
                      break;
                    }
                  }
                },
                selectedItem: selecteditem2,
              ),
            );
          }),
          SizedBox(
            height: MediaHeight * 0.02,
          ),
          Padding(
            padding: const EdgeInsets.all(4.0),
            child: SelectWeekDays(
              fontSize: 14,
              fontWeight: FontWeight.w500,
              days: _days,
              border: false,
              boxDecoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30.0),
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  colors: [
                    Color(0xFF2c8e82),
                    Color(0xFF42dc8f),
                  ],
                  tileMode:
                      TileMode.repeated, // repeats the gradient over the canvas
                ),
              ),
              onSelect: (values) {
                busObject.workingDaysStrings = values;
              },
            ),
          ),
          SizedBox(
            height: MediaHeight * 0.02,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                children: [
                  Container(
                    child: Text(
                      '${time.hour}',
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                  ElevatedButton(
                      onPressed: () async {
                        TimeOfDay? newTime = await showTimePicker(
                            context: context,
                            initialTime: time,
                            builder: (context, child) {
                              return MediaQuery(
                                  data: MediaQuery.of(context)
                                      .copyWith(alwaysUse24HourFormat: true),
                                  child: child!);
                            });
                        if (newTime == null) return;

                        setState(() {
                          time = newTime;
                          print(newTime);
                        });

                        busObject.stHour = time.hour;
                      },
                      style:
                          ElevatedButton.styleFrom(primary: Color(0xFF32bd8d)),
                      child: Text('Start Hour')),
                ],
              ),
              Column(
                children: [
                  Container(
                    child: Text(
                      '${timeEnd.hour}',
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                  ElevatedButton(
                      onPressed: () async {
                        TimeOfDay? newTime = await showTimePicker(
                            context: context,
                            initialTime: timeEnd,
                            builder: (context, child) {
                              return MediaQuery(
                                  data: MediaQuery.of(context)
                                      .copyWith(alwaysUse24HourFormat: true),
                                  child: child!);
                            });
                        if (newTime == null) return;

                        setState(() {
                          timeEnd = newTime;
                          print(timeEnd);
                        });

                        busObject.enHour = timeEnd.hour;
                      },
                      style:
                          ElevatedButton.styleFrom(primary: Color(0xFF32bd8d)),
                      child: Text('End Hour'))
                ],
              ),
            ],
          ),
          SizedBox(height: MediaHeight * 0.03),
          Container(
            child: Text(
              'Active Status',
              style: TextStyle(fontSize: 16),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            child: Center(
              child: FlutterSwitch(
                activeColor: Color(0xFF32bd8d),
                width: 130,
                height: 40.0,
                valueFontSize: 20.0,
                toggleSize: 30,
                value: free,
                activeText: 'Active',
                borderRadius: 30.0,
                padding: 8.0,
                showOnOff: true,
                onToggle: (val) {
                  setState(() {
                    busObject.free = val;
                    free = val;
                  });
                },
              ),
            ),
          ),
          SizedBox(height: MediaHeight * 0.05),
          FloatingActionButton(
            backgroundColor: Color(0xFF32bd8d),
            onPressed: () async {
              List<BusModel> temp;
              temp = Provider.of<BusAPI>(context, listen: false).BusesList;
              var x = await http.editBus(
                  id: busObject.id,
                  startingStation: busObject.startingStation,
                  route: busObject.route,
                  workingDays: busObject.workingDaysStrings,
                  stHour: busObject.stHour,
                  enHour: busObject.enHour,
                  free: busObject.free,
                  currentId: currentId,
                  token: Provider.of<SignInProvider>(context, listen: false)
                      .token);
              if (x == 'Done Successfully') {
                showSuccess(x);
              } else {
                showError(x);
              }
            },
            child: Icon(Icons.check),
          ),
          SizedBox(height: MediaHeight * 0.05),
        ],
      ),
    );
  }
}
