import 'package:admin/Components/constants.dart';
import 'package:admin/Models/news_model.dart';
import 'package:admin/Screens/HomePage/newwelcom.dart';
import 'package:admin/service/signin_api.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import '../Components/Search_bar.dart';
import '../service/http.dart';


String title = '';
String newNews = '';
String editTitle = '';
String editNewNews = '';
String currentTitle = '';
List<dynamic> temp = [];

class NewsScreen extends StatefulWidget {
  static String id = 'newsScreen';

  @override
  State<NewsScreen> createState() => NewsScreenState();
}

class NewsScreenState extends State<NewsScreen> {
  void showError(String message) {
    showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: Text('Something went wrong'),
          content: Text(message),
          actions: [
            MaterialButton(
                child: Text(
                  'Okay',
                  style: TextStyle(color: Colors.white),
                ),
                color: mainColor,
                onPressed: () {
                  Navigator.of(context, rootNavigator: true).pop();
                }),
          ],
        ));
  }

  void showSuccess(String message) {
    showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: Text('Great Work!'),
          content: Text(message),
          actions: [
            MaterialButton(
                child: Text(
                  'Okay',
                  style: TextStyle(color: Colors.white),
                ),
                color: mainColor,
                onPressed: () {
                  Navigator.of(context, rootNavigator: true).pushReplacement(MaterialPageRoute(builder: (context)=>Welcome()));

                }),
          ],
        ));
  }
  @override
  Widget build(BuildContext context) {
    var MediaHeight = MediaQuery.of(context).size.height;
    var MediaWidth = MediaQuery.of(context).size.width;
    Http http = new Http();

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showModalBottomSheet(
              context: context,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.vertical(
                    top: Radius.circular(40),
                  )),
              builder: (context) => Container(
                height: MediaHeight,
                width: MediaWidth,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(40),
                        topLeft: Radius.circular(40)),
                    border: Border.all(
                        color: Color(0xFF32bd8d), width: 10)),
                child: Column(
                  children: [
                    SizedBox(
                      height: MediaHeight * 0.03,
                    ),
                    Container(
                      height: MediaHeight * 0.06,
                      width: MediaWidth * 0.8,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                          BorderRadius.all(Radius.circular(22)),
                          border: Border.all(
                            color: Color(0xFF32bd8d),
                          )),
                      child: TextField(
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(20)),
                              borderSide: BorderSide(
                                  width: 2,
                                  color: Color(0xff33C58E))),
                          labelText: 'New News Title',
                          hintText: 'Must be more than 3 characters',
                          labelStyle: TextStyle(color: Colors.black),
                          prefixIcon: Icon(
                            Icons.emergency_sharp,
                            color: Color(0xff33C58E),
                          ),
                          border: OutlineInputBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(20)),
                          ),
                        ),
                        onChanged: (value) {
                          title = value;
                        },
                      ),
                    ),
                    SizedBox(
                      height: MediaHeight * 0.03,
                    ),
                    Container(
                      height: MediaHeight * 0.06,
                      width: MediaWidth * 0.8,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                          BorderRadius.all(Radius.circular(40)),
                          border: Border.all(
                            color: Color(0xFF32bd8d),
                          )),
                      child: TextField(
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(20)),
                              borderSide: BorderSide(
                                  width: 2,
                                  color: Color(0xff33C58E))),
                          labelText: 'Add New News',
                          hintText: 'Must be more than 3 characters',
                          labelStyle: TextStyle(color: Colors.black),
                          prefixIcon: Icon(
                            Icons.emergency_sharp,
                            color: Color(0xff33C58E),
                          ),
                          border: OutlineInputBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(20)),
                          ),
                        ),
                        onChanged: (value) {
                          newNews = value;
                        },
                      ),
                    ),
                    SizedBox(
                      height: MediaHeight * 0.03,
                    ),
                    IconButton(
                      onPressed: () async {
                        var x = await http.addNews(
                            title: title,
                            newNews: newNews,
                            token: Provider.of<SignInProvider>(
                                context,
                                listen: false)
                                .token);
                        print(x);
                        if (x == 'Done Successfully') {
                          showSuccess('Done Successfully');
                        } else {
                          showError(x);
                        }
                      },
                      icon: Icon(
                        Icons.send,
                        color: Color(0xFF32bd8d),
                      ),
                    )
                  ],
                ),
              ));
        },
        child: Icon(Icons.add),
        backgroundColor: Color(0xFF32bd8d),
      ),
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'News',
          style: GoogleFonts.montserrat(fontWeight: FontWeight.w500),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: NewsBody(),
    );
  }
}

class NewsBody extends StatefulWidget {
  @override
  State<NewsBody> createState() => _NewsBodyState();
}

class _NewsBodyState extends State<NewsBody> {
  late List<News> news = [];
  String query = '';

  @override
  void initState() {
    super.initState();

    news = Provider.of<newsAPI>(context, listen: false).newsList;
  }

  @override
  Widget build(BuildContext context1) {

    return Column(
      children: [
        Stack(
          children: [
            Container(
              margin: EdgeInsets.only(
                  bottom: MediaQuery.of(context).size.height / 22),
              height: MediaQuery.of(context).size.height / 6,
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25)),
                  gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      stops: [
                        0.02,
                        0.5,
                      ],
                      colors: [
                        Color(0xFF2c8e82),
                        Color(0xFF42dc8f),
                      ])),
            ),
            Positioned(
              child: SearchBar(),
              bottom: 0,
              left: 0,
              right: 0,
            ),
          ],
        ),
        SizedBox(
          height: 15,
        ),
        Expanded(
          //Listview
          child: MediaQuery.removePadding(
            context: context,
            removeTop: true,
            child: ListView.builder(
              //shrinkWrap: true,
              itemCount: news.length,
              itemBuilder: (context, index) {
                final news1 = news[index];
                return buildNews(news1, context1, index);
              },
            ),
          ),
        ),
      ],
    );
  }

  Widget SearchBar() => SearchWidget(
    text: query,
    hintText: 'Search for a news..',
    onChanged: searchNews,
  );

  void searchNews(String query) {
    final news =
    Provider.of<newsAPI>(context, listen: false).newsList.where((news) {
      final nameLower = news.title.toLowerCase();
      final searchLower = query.toLowerCase();

      return nameLower.contains(searchLower);
    }).toList();

    setState(() {
      this.query = query;
      this.news = news;
    });
  }

  Widget buildNews(News news, BuildContext context, int index) {
    var MediaHeight = MediaQuery.of(context).size.height;
    var MediaWidth = MediaQuery.of(context).size.width;
    Http http = new Http();
    temp = Provider.of<newsAPI>(context, listen: false).newsList;
    void showError(String message) {
      showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text('Something went wrong'),
            content: Text(message),
            actions: [
              MaterialButton(
                  child: Text(
                    'Okay',
                    style: TextStyle(color: Colors.white),
                  ),
                  color: mainColor,
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pop();
                  }),
            ],
          ));
    }

    void showSuccess(String message) {
      showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text('Great Work!'),
            content: Text(message),
            actions: [
              MaterialButton(
                  child: Text(
                    'Okay',
                    style: TextStyle(color: Colors.white),
                  ),
                  color: mainColor,
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pushReplacement(MaterialPageRoute(builder: (context)=>Welcome()));

                  }),
            ],
          ));
    }
    return ListTile(
        leading: SvgPicture.asset(
          'assets/newsIcon.svg',
          color: mainColor,
          fit: BoxFit.cover,
          width: 50,
          height: 50,
        ),
        title: Text(
          news.title,
          style: GoogleFonts.montserrat(fontWeight: FontWeight.w500),
        ),
        subtitle: Text('Click to see details'),
        onTap: () {

          editTitle = temp[index].title;
          editNewNews = temp[index].newNews;
          currentTitle = editTitle;
          showModalBottomSheet(
              context: context,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.vertical(
                      top: Radius.circular(40))),
              builder: (context) => Container(
                height: MediaHeight,
                width: MediaWidth,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(40),
                        topLeft: Radius.circular(40)),
                    border: Border.all(
                        color: Color(0xFF32bd8d),
                        width: 10)),
                child: Column(
                  children: [
                    SizedBox(
                      height: MediaHeight * 0.03,
                    ),
                    Text(
                      'Edit News',
                      style: GoogleFonts.montserrat(
                          color: Colors.black,
                          fontSize: 20,
                          fontWeight:
                          FontWeight.w500),
                    ),
                    SizedBox(
                      height: MediaHeight * 0.03,
                    ),
                    Container(
                      height: MediaHeight * 0.06,
                      width: MediaWidth * 0.8,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                          BorderRadius.all(
                              Radius.circular(
                                  22)),
                          border: Border.all(
                            color: Color(0xFF32bd8d),
                          )),
                      child: TextField(
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                              borderRadius:
                              BorderRadius.all(
                                  Radius.circular(
                                      20)),
                              borderSide: BorderSide(
                                  width: 2,
                                  color: Color(
                                      0xff33C58E))),
                          hintText:
                          '${temp[index].title}',
                          labelStyle: TextStyle(
                              color: Colors.black),
                          prefixIcon: Icon(
                            Icons.emergency_sharp,
                            color: Color(0xff33C58E),
                          ),
                          border: OutlineInputBorder(
                            borderRadius:
                            BorderRadius.all(
                                Radius.circular(
                                    20)),
                          ),
                        ),
                        onChanged: (value) {
                          editTitle = value;
                        },
                      ),
                    ),
                    SizedBox(
                      height: MediaHeight * 0.03,
                    ),
                    Container(
                      height: MediaHeight * 0.06,
                      width: MediaWidth * 0.8,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                          BorderRadius.all(
                              Radius.circular(
                                  22)),
                          border: Border.all(
                            color: Color(0xFF32bd8d),
                          )),
                      child: TextField(
                        maxLines: 3,
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                              borderRadius:
                              BorderRadius.all(
                                  Radius.circular(
                                      20)),
                              borderSide: BorderSide(
                                  width: 2,
                                  color: Color(
                                      0xff33C58E))),
                          hintText:
                          '${temp[index].newNews}',
                          labelStyle: TextStyle(
                              color: Colors.black),
                          prefixIcon: Icon(
                            Icons.emergency_sharp,
                            color: Color(0xff33C58E),
                          ),
                          border: OutlineInputBorder(
                            borderRadius:
                            BorderRadius.all(
                                Radius.circular(
                                    20)),
                          ),
                        ),
                        onChanged: (value) {
                          editNewNews = value;
                        },
                      ),
                    ),
                    SizedBox(
                      height: MediaHeight * 0.03,
                    ),
                    Row(
                      mainAxisAlignment:
                      MainAxisAlignment.center,
                      children: [
                        IconButton(
                          onPressed: () async {
                            print(
                                '${editTitle} + ${editNewNews} + $currentTitle');
                            var x = await http.editNews(
                                title: editTitle,
                                newNews: editNewNews,
                                currentTitle:
                                currentTitle,
                                token: Provider.of<
                                    SignInProvider>(
                                    context,
                                    listen: false)
                                    .token);
                            if (x == 'Done Successfully') {
                              showSuccess('Done Successfully');
                            } else {
                              showError(x);
                            }
                          },
                          icon: Icon(
                            Icons.send,
                            color: Color(0xFF32bd8d),
                          ),
                        ),
                        SizedBox(
                          width: MediaWidth * 0.05,
                        ),
                        IconButton(
                          onPressed: () async {
                            var x = await http.deleteNews(
                                currentTitle:
                                currentTitle,
                                token: Provider.of<
                                    SignInProvider>(
                                    context,
                                    listen: false)
                                    .token);
                            if (x == 'Done Successfully') {
                              showSuccess('Done Successfully');
                            } else {
                              showError(x);
                            }
                          },
                          icon: Icon(
                            Icons.delete,
                            color: Color(0xFF32bd8d),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ));
        });
  }
}
