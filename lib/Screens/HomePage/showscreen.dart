import 'package:admin/Screens/HomePage/widgets.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:admin/Models/bus_model.dart';
import 'package:admin/Models/driver_model.dart';
import 'package:admin/Models/station_model.dart';
import 'package:admin/Models/trips_model.dart';
import 'package:admin/Screens/Drivers/Drivers.dart';
import 'package:admin/Screens/Loading_indicator.dart';
import 'package:admin/Screens/Buses/buses.dart';
import 'package:admin/Screens/Stations/stations.dart';
import 'package:admin/Screens/Trips/trips.dart';
import 'package:admin/service/signin_api.dart';
class showsc extends StatefulWidget {
  const showsc({Key? key}) : super(key: key);
  @override
  State<showsc> createState() => _showscState();
}
class _showscState extends State<showsc> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.only(top:90),
            child: ListView(
              children: [
                GestureDetector( onTap: ()async {
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) =>
                          new LoadingScreenIndicator()));
                  await Provider.of<StationModel>(context, listen: false)
                      .getStations(
                      token: Provider.of<SignInProvider>(context,
                          listen: false)
                          .token);
                  await Provider.of<TripAPI>(context, listen: false)
                      .getTrips(
                      token: Provider.of<SignInProvider>(context,
                          listen: false)
                          .token);
                  await Provider.of<BusAPI>(context, listen: false)
                      .getFreeBuses(
                      token: Provider.of<SignInProvider>(context,
                          listen: false)
                          .token);
                  Route route =
                  MaterialPageRoute(builder: (context) => Trips());
                  Navigator.pushReplacement(context, route);
                }, child: Listbut('show Trips')),
                GestureDetector( onTap: ()async {
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) =>
                          new LoadingScreenIndicator()));
                  await Provider.of<StationModel>(context, listen: false)
                      .getStations(
                      token: Provider.of<SignInProvider>(context,
                          listen: false)
                          .token);
                  await Provider.of<TripAPI>(context, listen: false)
                      .getTrips(
                      token: Provider.of<SignInProvider>(context,
                          listen: false)
                          .token);
                  await Provider.of<BusAPI>(context, listen: false)
                      .getFreeBuses(
                      token: Provider.of<SignInProvider>(context,
                          listen: false)
                          .token);
                  await Provider.of<BusAPI>(context, listen: false)
                      .getBuses();
                  Route route =
                  MaterialPageRoute(builder: (context) => Buses());
                  Navigator.pushReplacement(context, route);
                }, child: Listbut('show Buses')),
                GestureDetector( onTap: ()async {
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) =>
                          new LoadingScreenIndicator()));
                  await Provider.of<StationModel>(context, listen: false)
                      .getStations(
                      token: Provider.of<SignInProvider>(context,
                          listen: false)
                          .token);
                  Route route =
                  MaterialPageRoute(builder: (context) => Stations());
                  Navigator.pushReplacement(context, route);
                  ;
                }, child: Listbut('show Stations')),
                GestureDetector( onTap: ()async{
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) =>
                          new LoadingScreenIndicator()));
                  await Provider.of<driverAPI>(context, listen: false)
                      .getDrivers(
                      token: Provider.of<SignInProvider>(context,
                          listen: false)
                          .token);
                  await Provider.of<BusAPI>(context, listen: false)
                      .getFreeBuses(
                      token: Provider.of<SignInProvider>(context,
                          listen: false)
                          .token);
                  Route route =
                  MaterialPageRoute(builder: (context) => Drivers());
                  Navigator.pushReplacement(context, route);
                }, child: Listbut('show Drivers')),

              ],
            ),
          ),
          Hero(tag: 1, child: sidebar())
        ],
      ),
    );
  }
}
