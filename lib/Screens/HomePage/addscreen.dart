import 'package:admin/Screens/HomePage/widgets.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:admin/Models/bus_model.dart';
import 'package:admin/Models/station_model.dart';
import 'package:admin/Models/trips_model.dart';
import 'package:admin/Screens/Buses/add_bus.dart';
import 'package:admin/Screens/Loading_indicator.dart';
import 'package:admin/Screens/Drivers/MakeDriver.dart';
import 'package:admin/Screens/Trips/add_trip.dart';
import 'package:admin/Screens/Stations/add_station.dart';
import 'package:admin/service/signin_api.dart';

class addsc extends StatelessWidget {
  const addsc({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: true,
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 90),
              child: ListView(
                children: [
                  GestureDetector( onTap: ()async {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) =>
                            new LoadingScreenIndicator()));
                    await Provider.of<StationModel>(context, listen: false)
                        .getStations(
                        token: Provider.of<SignInProvider>(context,
                            listen: false)
                            .token);
                    await Provider.of<TripAPI>(context, listen: false)
                        .getTrips(
                        token: Provider.of<SignInProvider>(context,
                            listen: false)
                            .token);
                    await Provider.of<BusAPI>(context, listen: false)
                        .getFreeBuses(
                        token: Provider.of<SignInProvider>(context,
                            listen: false)
                            .token);

                    Route route =
                    MaterialPageRoute(builder: (context) => add_trip());
                    Navigator.pushReplacement(context, route);
                  }, child: Listbut('Add Trips')),
                  GestureDetector( onTap: ()async {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) =>
                            new LoadingScreenIndicator()));
                    await Provider.of<StationModel>(context, listen: false)
                        .getStations(
                        token: Provider.of<SignInProvider>(context,
                            listen: false)
                            .token);
                    await Provider.of<TripAPI>(context, listen: false)
                        .getTrips(
                        token: Provider.of<SignInProvider>(context,
                            listen: false)
                            .token);
                    Route route =
                    MaterialPageRoute(builder: (context) => add_bus());
                    Navigator.pushReplacement(context, route);
                  }, child: Listbut('Add Buses')),
                  GestureDetector( onTap: (){
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new add_station()));
                  }, child: Listbut('Add Stations')),
                  GestureDetector( onTap: () async {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) =>
                            new LoadingScreenIndicator()));
                    await Provider.of<BusAPI>(context, listen: false)
                        .getFreeBuses(
                        token: Provider.of<SignInProvider>(context,
                            listen: false)
                            .token);
                    Route route =
                    MaterialPageRoute(builder: (context) => MakeDriver());
                    Navigator.pushReplacement(context, route);

                  }, child: Listbut('Add Drivers')),
                ],
              ),
            ),
            Hero( tag: 1, child: sidebar()),
          ],
        ),
      ),
    );
  }
}
