import 'package:admin/Screens/complaints.dart';
import 'package:admin/Screens/news.dart';
import 'package:admin/service/signin_api.dart';
import 'package:admin/Screens/HomePage/showscreen.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../Models/complaints_model.dart';
import '../../Models/news_model.dart';
import '../Loading_indicator.dart';
import '../sign_in.dart';
import 'addscreen.dart';

//Iconbutton
class Iconbut extends StatelessWidget {
  String text;
  IconData icon;
  double fonsize;

  Iconbut(this.text, this.icon, this.fonsize);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 110),
      child: Column(children: [
        Text(
          text,
          style: TextStyle(
              decoration: TextDecoration.none,
              color: Colors.white,
              fontSize: fonsize,
              fontWeight: FontWeight.bold),
        ),
        const SizedBox(
          height: 10,
        ),
        Icon(
          icon,
          color: Colors.white,
        ),
      ]),
    );
  }
}

class but extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(20),
      child: Container(
        decoration: BoxDecoration(),
      ),
    );
  }
}

//sidebar
class sidebar extends StatelessWidget {
  const sidebar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        decoration: const BoxDecoration(
          color: Color(0xFF32bd8d),
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(20), bottomRight: Radius.circular(20)),
        ),
        width: 80,
        child: ListView( children: [
          GestureDetector(onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>const addsc())), child: Iconbut('Add', Icons.add, 18)),
          GestureDetector(onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>const showsc())), child: Iconbut('show ', Icons.visibility, 18)),
          GestureDetector(onTap: ()async {
            Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (context) =>
                    new LoadingScreenIndicator()));
            await Provider.of<ComplaintsAPI>(context, listen: false)
                .getComplains(
                token: Provider.of<SignInProvider>(context,
                    listen: false)
                    .token);
            Route route = MaterialPageRoute(
                builder: (context) => Complaints());
            Navigator.pushReplacement(context, route);
          }, child: Iconbut('Complaints ', Icons.face_outlined, 12)),
          GestureDetector(onTap: ()async {
            Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (context) =>
                    new LoadingScreenIndicator()));
            await Provider.of<newsAPI>(context, listen: false)
                .getNews(
                token: Provider.of<SignInProvider>(context,
                    listen: false)
                    .token);
            Route route =
            MaterialPageRoute(builder: (context) => NewsScreen());
            Navigator.pushReplacement(context, route);
          }, child: Iconbut('News ', Icons.newspaper, 14)),
          GestureDetector(onTap: ()async{
            SharedPreferences prefs =
            await SharedPreferences.getInstance();
            prefs.clear();
            Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (BuildContext context) {
                return SignIn();
              },
            ));
          }, child: Iconbut('logout ', Icons.logout, 18)),
        ]),
      ),
    );
  }
}

//ListBut
class Listbut extends StatefulWidget {
  String text;
  Listbut(this.text);

  @override
  State<Listbut> createState() => _ListbutState();
}

class _ListbutState extends State<Listbut> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 100, top: 30, right: 30),
      height: MediaQuery.of(context).size.height * 0.15,
      width: MediaQuery.of(context).size.width * 0.6,
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(20),
        ),
        color: Color(0xFF32bd8d),
      ),
      child: Center(
        child: Text(
          '${widget.text}',
          style: GoogleFonts.montserrat(
              fontSize: 22, color: Colors.white, fontWeight: FontWeight.w500),
        ),
      ),
    );
  }
}

//welcome text container
class Welcomtext extends StatelessWidget {
  const Welcomtext({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 90, right: 50),
      child: Text(
        ' Welcome to Harakeh , what would you like to do today ?',
        style: GoogleFonts.montserrat(
            fontSize: 18, color: Colors.black, fontWeight: FontWeight.w500),
      ),
    );
  }
}
