import 'package:admin/Components/constants.dart';
import 'package:admin/Models/station_model.dart';
import 'package:admin/service/http.dart';
import 'package:admin/service/signin_api.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';

import '../HomePage/newwelcom.dart';
List<dynamic> temp = [];
late var currentName ;
late var currentLat ;
late var currentLng ;
late var currentMain ;
class StationDetails extends StatefulWidget {
  final int index;
  static String id = 'trip_details';

  const StationDetails({required this.index});

  @override
  State<StationDetails> createState() => _StationDetailsState(this.index);
}

class _StationDetailsState extends State<StationDetails> {
  final int index;
  // late String currentName;
  // late double currentLng;
  // late double currentLat;
  late GoogleMapController myMapController;
  List<dynamic> temp = [];
  void showError(String message) {
    showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: Text('Something went wrong'),
          content: Text(message),
          actions: [
            MaterialButton(
                child: Text(
                  'Okay',
                  style: TextStyle(color: Colors.white),
                ),
                color: mainColor,
                onPressed: () {
                  Navigator.of(context, rootNavigator: true).pop();
                }),
          ],
        ));
  }

  void showSuccess(String message) {
    showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: Text('Great Work!'),
          content: Text(message),
          actions: [
            MaterialButton(
                child: Text(
                  'Okay',
                  style: TextStyle(color: Colors.white),
                ),
                color: mainColor,
                onPressed: () {
                  Navigator.of(context, rootNavigator: true).pushReplacement(MaterialPageRoute(builder: (context)=>Welcome()));

                }),
          ],
        ));
  }
  @override
  _StationDetailsState(this.index);

  void initState() {
    super.initState();

    temp = Provider.of<StationModel>(context , listen:  false ).StationList;
     currentName = temp[index].name;
     currentLat = temp[index].lat;
     currentLng = temp[index].lng;
     currentMain = temp[index].MainLocation;

  }

  Widget build(BuildContext context) {
    var MediaHeight = MediaQuery.of(context).size.height;
    var MediaWidth = MediaQuery.of(context).size.width;

    Http http = new Http();
    return Scaffold(
      appBar: AppBar(
        title: Text('Station Site'),
        backgroundColor: Color(0xFF32bd8d),
        actions: [
          IconButton(
              onPressed: () async{
                print( ' ${currentLat} + ${currentLng} + ${currentMain}' );
                var x= await http.deleteStation(currentStation: currentName ,token: Provider.of<SignInProvider>(context, listen: false).token);
                if(x=='Done Successfully')
                {
                  showSuccess(x);
                }
                else
                {
                  showError(x);
                }
              },
              icon: Icon(
                Icons.delete,
              ))
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: MediaHeight * 0.01,
          ),
          Padding(
            padding: EdgeInsets.only(  top: MediaHeight * 0.01,
                left: MediaWidth * 0.07,
                right: MediaWidth * 0.07),
            child: TextFormField(
              decoration: InputDecoration(
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    borderSide:
                        BorderSide(width: 2, color: Color(0xff33C58E))),
                hintText:temp[index].name,
                labelStyle: TextStyle(color: Colors.black),
                prefixIcon: Icon(
                  Icons.drive_file_rename_outline,
                  color: Color(0xff33C58E),
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                ),
              ),
              onChanged: (String value) {
                temp[index].name = value;
              },
              keyboardType: TextInputType.text,
            ),
          ),
          SizedBox(
            height: MediaHeight * 0.01,
          ),
          Consumer<StationModel>(builder: (context , station2 , child){
            return Expanded(
              child: GoogleMap(
                initialCameraPosition: CameraPosition(
                  target: station2.StationList[index].MainLocation,
                  zoom: 17.0,
                ),
                markers: station2.StationList[index].myMarker(),
                mapType: MapType.normal,
                onMapCreated: (controller) {
                  myMapController = controller;
                },
                onTap: (LatLng latLng) {
                  station2.StationList[index].markers.clear();
                  station2.Setlonglat(latLng.latitude, latLng.longitude, station2.StationList[index]);
                },
              ),
            );
          })

        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {

          var x= await http.editStaion(
              name: temp[index].name,
              lat: temp[index].lat,
              lng:temp[index].lng,
              token: Provider.of<SignInProvider>(context, listen: false).token,
              currentName: currentName
          );
          if(x=='Done Successfully')
          {
            showSuccess(x);
          }
          else
          {
            showError(x);
          }
        },
        child: Icon(Icons.check),
        backgroundColor: Color(0xFF32bd8d),
      ),
    );
  }
}

class StationDetailsBody extends StatefulWidget {
  final int index;
  final Station station;
  StationDetailsBody(this.station, this.index);
  @override
  State<StationDetailsBody> createState() =>
      _StationDetailsBodyState(station, index);
}

class _StationDetailsBodyState extends State<StationDetailsBody> {
  final int index;
  late List<dynamic> stations;
  final Station station;
  _StationDetailsBodyState(this.station, this.index);
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height / 4.8,
      child: Column(
        children: [
          SizedBox(
            height: 25,
          ),
          Expanded(
            child: ListView.separated(
              padding: EdgeInsets.all(15),
              scrollDirection: Axis.horizontal,
              itemCount: 1,
              itemBuilder: (context, index) {
                // final trip = trips[index];
                return buildStation(station, context, index);
              },
              separatorBuilder: (BuildContext context, int index) {
                return const SizedBox(width: 10);
              },
            ),
          ),
        ],
      ),
    );
  }
}

Widget buildStation(Station station, BuildContext context, int index) {
  return Column(
    children: [
      Image.asset('assets/Road.png',
          height: MediaQuery.of(context).size.height / 10,
          width: MediaQuery.of(context).size.height / 2.5),
      SizedBox(
        height: 8,
      ),
      Text('${station.name}',
          style: GoogleFonts.montserrat(fontWeight: FontWeight.w500))
    ],
  );
}
