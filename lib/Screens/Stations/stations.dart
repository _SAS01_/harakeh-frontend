import 'package:admin/Models/station_model.dart';
import 'package:admin/Screens/Stations/station_details.dart';
import 'package:admin/service/http.dart';
import 'package:admin/service/signin_api.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:admin/Components/my_drawer.dart';
import '../../Components/search_bar.dart';
Http http = new Http();
class Stations extends StatefulWidget {
  static String id = 'stations';

  @override
  State<Stations> createState() => StationsState();
}

class StationsState extends State<Stations> {

  @override

  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Stations',
          style: GoogleFonts.montserrat(fontWeight: FontWeight.w500),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: StationsBody(),
    );
  }
}

class StationsBody extends StatefulWidget {
  @override
  State<StationsBody> createState() => _StationsBodyState();
}

class _StationsBodyState extends State<StationsBody> {
  String query = '';
  late List<dynamic> stations;

  @override
  void initState()  {
    super.initState();
    Provider.of<StationModel>(context, listen: false).getStations(token: Provider.of<SignInProvider>(context, listen: false).token);
    stations = Provider.of<StationModel>(context, listen: false).StationList;
  }
@override
void didChangeDependencies() {
    super.didChangeDependencies();
    Provider.of<StationModel>(context, listen: false).getStations(token: Provider.of<SignInProvider>(context, listen: false).token);
    stations = Provider.of<StationModel>(context, listen: false).StationList;

}

  @override
  Widget build(BuildContext context1) {
    return Column(
      children: [
        Stack(
          children: [
            Container(
              margin: EdgeInsets.only(
                  bottom: MediaQuery.of(context).size.height / 22),
              height: MediaQuery.of(context).size.height / 6,
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25)),
                  gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      stops: [
                        0.02,
                        0.5,
                      ],
                      colors: [
                        Color(0xFF2c8e82),
                        Color(0xFF42dc8f),
                      ])),
            ),
            Positioned(
              child: SearchBar(),
              bottom: 0,
              left: 0,
              right: 0,
            ),
          ],
        ),
        SizedBox(
          height: 15,
        ),
        Expanded(
          //Listview
          child: MediaQuery.removePadding(
            context: context,
            removeTop: true,

            child: ListView.builder(
              //shrinkWrap: true,
              itemCount: stations.length,
              itemBuilder: (context, index) {
                final station = stations[index];
                return buildTrip(station, context1, index);
              },
            ),
          ),
        ),
      ],
    );
  }

  Widget SearchBar() => SearchWidget(
    text: query,
    hintText: 'Search for a trip..',
    onChanged: searchTrip,
  );

  void searchTrip(String query) {
    final stations =
    Provider.of<StationModel>(context, listen: false).StationList.where((station) {
      final nameLower = station.name.toLowerCase();
      final searchLower = query.toLowerCase();

      return nameLower.contains(searchLower);
    }).toList();

    setState(() {
      this.query = query;
      this.stations = stations;
    });
  }

  Widget buildTrip(Station station, BuildContext context, int index) {
    return ListTile(
        leading: Image.asset(
          'assets/marker.png',
          fit: BoxFit.fitHeight,
          width: 40,
          height: 40,
        ),
        title: Text(
          station.name,
          style: GoogleFonts.montserrat(fontWeight: FontWeight.w500),
        ),
        subtitle: Text('Click to edit'),
        onTap: () {

          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return StationDetails(
              index: index,
            );
          }
          ));
        }
        );
  }
}
