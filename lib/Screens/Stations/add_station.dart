import 'package:admin/Models/station_model.dart';
import 'package:admin/service/http.dart';
import 'package:admin/service/signin_api.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';

import '../../Components/constants.dart';
import '../HomePage/newwelcom.dart';
Station station2 = new Station(lat: 33.5138, lng:  36.2765, name: '', MainLocation: LatLng(33.5138, 36.2765));
class add_station extends StatefulWidget {
  static String id = 'show map';
  @override
  _add_stationState createState() => _add_stationState();
}

class _add_stationState extends State<add_station> {
  late GoogleMapController myMapController;
  void showError(String message) {
    showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
              title: Text('Something went wrong'),
              content: Text(message),
              actions: [
                MaterialButton(
                    child: Text(
                      'Okay',
                      style: TextStyle(color: Colors.white),
                    ),
                    color: mainColor,
                    onPressed: () {
                      Navigator.of(context, rootNavigator: true).pop();
                    }),
              ],
            ));
  }

  void showSuccess(String message) {
    showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
              title: Text('Great Work!'),
              content: Text(message),
              actions: [
                MaterialButton(
                    child: Text(
                      'Okay',
                      style: TextStyle(color: Colors.white),
                    ),
                    color: mainColor,
                    onPressed: () {
                      Navigator.of(context, rootNavigator: true).pushReplacement(MaterialPageRoute(builder: (context)=>Welcome()));
                    }),
              ],
            ));
  }

  @override
  void initState() {
    super.initState();

  }

  Widget build(BuildContext context) {
    var MediaHeight = MediaQuery.of(context).size.height;
    var MediaWidth = MediaQuery.of(context).size.width;

    Http http = new Http();
    String Station_name = '';
    double lat;
    double lng;
    var x;
    return ChangeNotifierProvider(
      create: (context) {
        return StationModel();
      },
      child: Consumer<StationModel>(
        builder: (context, station, child) {
          return Scaffold(
              appBar: AppBar(
                centerTitle: true,
                title: Text('Add Station Site'),
                backgroundColor: Color(0xFF32bd8d),
              ),
              body: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: MediaHeight * 0.01,
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: MediaHeight * 0.01,
                        left: MediaWidth * 0.07,
                        right: MediaWidth * 0.07),
                    child: TextFormField(
                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                            borderSide:
                                BorderSide(width: 2, color: Color(0xff33C58E))),
                        labelText: 'Station Name',
                        labelStyle: TextStyle(color: Colors.black),
                        prefixIcon: Icon(
                          Icons.drive_file_rename_outline,
                          color: Color(0xff33C58E),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        ),
                      ),
                      validator: (value) {
                        if (value == null || value.length <= 5) {
                          return "Trip Name Should be 5 chars at least";
                        } else
                          return null;
                      },
                      onChanged: (String value) {
                       station2.name = value;
                       print(station2.name);
                      },
                      keyboardType: TextInputType.text,
                    ),
                  ),
                  SizedBox(
                    height: MediaHeight * 0.01,
                  ),
                  Expanded(
                    child: GoogleMap(
                      onTap: (LatLng latLng) {
                        station2.markers.clear();
                        station2.lat = latLng.latitude;
                        station2.lng = latLng.longitude;
                        station.Setlonglat(latLng.latitude, latLng.longitude  , station2);
                        print(station.mainLocation);

                      },
                      initialCameraPosition: CameraPosition(
                        target: station2.MainLocation,
                        zoom: 17.0,
                      ),
                      markers: station2.myMarker(),
                      mapType: MapType.normal,
                      onMapCreated: (controller) {
                        myMapController = controller;
                      },
                    ),
                  ),
                ],
              ),
              floatingActionButton: FloatingActionButton(
                onPressed: () async {
                  lng = station.mainLocation.longitude;
                  lat = station.mainLocation.latitude;
                  x = await http.add_station(
                      name: station2.name,
                      lat: station2.lat,
                      lng: station2.lng,
                      token: Provider.of<SignInProvider>(context, listen: false)
                          .token);
                  if (x == 'Done Successfully') {
                    showSuccess(x);
                  } else {
                    showError(x);
                  }
                },
                child: Icon(Icons.check),
                backgroundColor: Color(0xFF32bd8d),
              ));
        },
      ),
    );
  }
}
