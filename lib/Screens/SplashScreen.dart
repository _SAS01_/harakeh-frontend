import 'dart:async';
import 'package:admin/Screens/sign_in.dart';
import 'package:admin/service/signin_api.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:provider/provider.dart';

import 'HomePage/newwelcom.dart';

class SplashScreen extends StatefulWidget {
  static String id = "Splash Screen";
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    var auth = Provider.of<SignInProvider>(context, listen: false);
    super.initState();
    Timer(
        Duration(seconds: 3),
        () => Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => auth.isAuth ? Welcome() : SignIn())));
  }

  @override
  Widget build(BuildContext context) {
    var MediaHeight = MediaQuery.of(context).size.height;
    var MediaWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
          height: MediaHeight,
          width: MediaWidth,
          color: Color(0xFF42dc8f),
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: MediaWidth * 0.7,
              ),
              Container(
                child: Image.asset(
                  'assets/LogoWhite.png',
                  width: MediaWidth * 0.8,
                  height: MediaHeight * 0.2,
                ),
              ),
              SizedBox(height: MediaHeight * 0.03),
              Container(
                margin: EdgeInsets.all(10),
                child: Text(
                  'Welcome To Harakeh Admin App',
                  style: GoogleFonts.montserrat(
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                      color: Colors.white),
                ),
              ),
              Container(
                margin: EdgeInsets.all(10),
                child: Text(
                  'Please Wait ...',
                  style: GoogleFonts.montserrat(
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                      color: Colors.white),
                ),
              ),
              SizedBox(
                height: MediaHeight * 0.01,
              ),
              Container(
                height: MediaHeight * 0.05,
                width: MediaWidth * 0.1,
                child: LoadingIndicator(
                  indicatorType: Indicator.circleStrokeSpin,

                  /// Required, The loading type of the widget
                  colors: const [Colors.white],

                  /// Optional, The color collections
                  strokeWidth: 5,

                  /// Optional, The stroke of the line, only applicable to widget which contains line
                  backgroundColor: Color(0xFF42dc8f),

                  /// Optional, Background of the widget
                  pathBackgroundColor: Color(0xFF42dc8f),

                  /// Optional, the stroke backgroundColor
                ),
              ),
            ],
          )),
    );
  }
}
