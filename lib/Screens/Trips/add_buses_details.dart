import 'package:admin/Components/constants.dart';
import 'package:admin/Models/trips_model.dart';
import 'package:admin/service/http.dart';
import 'package:admin/service/signin_api.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:new_gradient_app_bar/new_gradient_app_bar.dart';
import 'package:provider/provider.dart';
import 'package:admin/Models/bus_model.dart';

import '../HomePage/newwelcom.dart';

List<String> Buses = [];

class add_buses_details extends StatefulWidget {
  static String id = 'add_buses_details';

  @override
  State<add_buses_details> createState() => _add_buses_detailsState();
}

class _add_buses_detailsState extends State<add_buses_details> {
  List<BusModel> temp = [];
  void showError(String message) {
    showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: Text('Something went wrong'),
          content: Text(message),
          actions: [
            MaterialButton(
                child: Text(
                  'Okay',
                  style: TextStyle(color: Colors.white),
                ),
                color: mainColor,
                onPressed: () {
                  Navigator.of(context, rootNavigator: true).pop();
                }),
          ],
        ));
  }

  void showSuccess(String message) {
    showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: Text('Great Work!'),
          content: Text(message),
          actions: [
            MaterialButton(
                child: Text(
                  'Okay',
                  style: TextStyle(color: Colors.white),
                ),
                color: mainColor,
                onPressed: () {
                  Navigator.of(context, rootNavigator: true).pushReplacement(MaterialPageRoute(builder: (context)=>Welcome()));

                }),
          ],
        ));
  }

  void initState() {
    super.initState();

    temp = Provider.of<BusAPI>(context, listen: false).availbleBuses;
    for (int i = 0; i < temp.length; i++) {
      Buses.add(temp[i].id.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    var MediaHeight = MediaQuery.of(context).size.height;
    var MediaWidth = MediaQuery.of(context).size.width;
    Http http = new Http();

    final _formKey4 = GlobalKey<FormState>();
    return ChangeNotifierProvider(
        create: (context) {},
        child: Consumer<TripAPI>(
          builder: (context, trip, child) {
            List<dynamic> controllerBuses =
                List<dynamic>.filled(trip.Trips.length + 1, '', growable: true);
            dynamic selecteditem = "Choose Bus Number";
            return Scaffold(
              floatingActionButton: FloatingActionButton(
                backgroundColor: Color(0xFF32bd8d),
                onPressed: () async {
                  for (int i = 0;
                      i < trip.Trips[trip.Trips.length - 1].numStations;
                      i++) {
                    trip.Trips[trip.Trips.length - 1].buses
                        .add(controllerBuses[i]);
                  }

                  final isValidForm = _formKey4.currentState!.validate();
                  if (isValidForm) {
                    var x = await http.add_trip(
                        name: trip.Trips[trip.Trips.length - 1].name,
                        stations: trip.Trips[trip.Trips.length - 1].stations,
                        times: trip.Trips[trip.Trips.length - 1].time_stations,
                        buses: trip.Trips[trip.Trips.length - 1].buses,
                        token:
                            Provider.of<SignInProvider>(context, listen: false)
                                .token);
                    print('sameer');
                    if (x == 'Done Successfully') {
                      showSuccess(x);
                    } else {
                      showError(x);
                    }
                  }
                },
                child: Icon(Icons.check),
              ),
              appBar: NewGradientAppBar(
                  leading: IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: Icon(Icons.keyboard_arrow_left),
                  ),
                  centerTitle: true,
                  //backgroundColor: Color(0xFF00D3A0),
                  elevation: 0,
                  title: Text(
                    'Buses Details',
                    style: GoogleFonts.montserrat(fontWeight: FontWeight.w500),
                  ),
                  gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: [
                        Color(0xFF2c8e82),
                        Color(0xFF42dc8f),
                      ])),

              backgroundColor: Colors.white,
              body: Form(
                key: _formKey4,
                child: Column(
                  children: [
                    SizedBox(
                      height: MediaHeight * 0.02,
                    ),
                    Expanded(
                      child: ListView.builder(
                          itemCount: trip.Trips[trip.Trips.length - 1].numBuses,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: EdgeInsets.only(
                                  top: MediaHeight * 0.01,
                                  left: MediaWidth * 0.07,
                                  right: MediaWidth * 0.07),
                              child: Column(
                                children: [
                                  DropdownButtonHideUnderline(
                                    child: DropdownSearch<String>(
                                      items: Buses,
                                      popupProps: PopupPropsMultiSelection.menu(
                                        showSelectedItems: true,
                                        showSearchBox: true,
                                      ),
                                      validator: (value) =>
                                          value == "Choose Bus Number"
                                              ? 'field required'
                                              : null,
                                      onChanged: (value) {
                                        selecteditem = value!;
                                        controllerBuses[index] = value;
                                      },
                                      selectedItem: selecteditem,
                                    ),
                                  ),
                                  SizedBox(
                                    height: MediaHeight * 0.03,
                                  ),
                                ],
                              ),
                            );
                          }),
                    ),
                    SizedBox(
                      height: MediaHeight * 0.03,
                    ),
                  ],
                ),
              ),
            );
          },
        ));
  }
}
