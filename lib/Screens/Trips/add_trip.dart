import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:new_gradient_app_bar/new_gradient_app_bar.dart';
import 'package:provider/provider.dart';
import 'package:admin/Models/trips_model.dart';
import 'package:admin/Screens/Trips/add_trip_stations_names.dart';

String name = '';
int numStations = 0;
int numBuses = 0;

class add_trip extends StatefulWidget {
  static String id = 'add_trip';
  static final GlobalKey<FormState> _formkey1 = GlobalKey<FormState>();
  @override
  State<add_trip> createState() => _add_tripState();
}

class _add_tripState extends State<add_trip> {
  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    var MediaHeight = MediaQuery.of(context).size.height;
    var MediaWidth = MediaQuery.of(context).size.width;

    return ChangeNotifierProvider(create: (context) {
      return Trip(
          name: '',
          time_stations: [],
          buses: [],
          numBuses: 0,
          numStations: 0,
          stations: []);
    }, child: Consumer<TripAPI>(
      builder: (context, trip, child) {
        return Scaffold(
          appBar:  NewGradientAppBar(
              leading: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(Icons.keyboard_arrow_left),
              ),
              centerTitle: true,
              //backgroundColor: Color(0xFF00D3A0),
              elevation: 0,
              title: Text(
                'Add Trip Details',
                style: GoogleFonts.montserrat(fontWeight: FontWeight.w500),
              ),
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                    Color(0xFF2c8e82),
                    Color(0xFF42dc8f),
                  ])),
          floatingActionButton: FloatingActionButton(
            backgroundColor: Color(0xFF32bd8d),
            onPressed: () {
              //print('${trip.TripList.length}');
              //print('${trip.TripList[trip.TripList.length-1].numStations}');
              // print('${hamdi.name} + ${hamdi.numStations} + ${hamdi.numBus}');
              print('$name + $numBuses + $numStations');
              if (add_trip._formkey1.currentState!.validate()) {
                trip.Trips.add(Trip(
                    name: name,
                    time_stations: [],
                    buses: [],
                    numBuses: numBuses,
                    numStations: numStations,
                    stations: []));
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new add_trip_details()));
              }
            },
            child: Icon(Icons.check),
          ),
          backgroundColor: Colors.white,
          body: Form(
            key: add_trip._formkey1,
            child: Container(
              color: Colors.white,
              child: Padding(
                padding: EdgeInsets.only(
                    top: MediaHeight * 0.1,
                    left: MediaWidth * 0.07,
                    right: MediaWidth * 0.07),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextFormField(
                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                            borderSide:
                                BorderSide(width: 2, color: Color(0xff33C58E))),
                        labelText: 'Trip Name',
                        labelStyle: TextStyle(color: Colors.black),
                        prefixIcon: Icon(
                          Icons.drive_file_rename_outline,
                          color: Color(0xff33C58E),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        ),
                      ),
                      validator: (value) {
                        if (value == null || value.length <= 5) {
                          return "Trip Name Should be 5 chars at least";
                        } else
                          return null;
                      },
                      onChanged: (String value) {
                        name = value;
                      },
                      keyboardType: TextInputType.text,
                    ),
                    SizedBox(
                      height: MediaHeight * 0.02,
                    ),
                    TextFormField(

                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                            borderSide:
                                BorderSide(width: 2, color: Color(0xff33C58E))),
                        labelText: 'Number Of Stations',
                        labelStyle: TextStyle(color: Colors.black),
                        prefixIcon: Icon(
                          Icons.add_business,
                          color: Color(0xff33C58E),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        ),
                      ),
                      validator: (value) {
                        int test = int.parse(value!);
                        if (test < 3 || value == null) {
                          return "Number of Stations should be 3 at least";
                        } else
                          return null;
                      },
                      onChanged: (String value) {
                        numStations = int.parse(value);
                      },
                      keyboardType: TextInputType.number,
                      textInputAction: TextInputAction.done,
                    ),
                    SizedBox(
                      height: MediaHeight * 0.02,
                    ),
                    TextFormField(

                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                            borderSide:
                                BorderSide(width: 2, color: Color(0xff33C58E))),
                        labelText: 'Number Of Buses',
                        labelStyle: TextStyle(color: Colors.black),
                        prefixIcon: Icon(
                          Icons.directions_bus_sharp,
                          color: Color(0xff33C58E),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        ),
                      ),
                      validator: (value) {
                        if (value == null)
                          return "Required Field";
                        else
                          return null;
                      },
                      onChanged: (String value) {
                        numBuses = int.parse(value);
                      },
                      keyboardType: TextInputType.number,
                      textInputAction: TextInputAction.done,
                    ),
                    SizedBox(
                      height: MediaHeight * 0.02,
                    ),
                    SizedBox(
                      height: MediaHeight * 0.02,
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    ));
  }
}
