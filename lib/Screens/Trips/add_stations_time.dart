import 'package:admin/Models/trips_model.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:new_gradient_app_bar/new_gradient_app_bar.dart';
import 'package:provider/provider.dart';
import 'package:admin/Screens/Trips/add_buses_details.dart';

class add_stations_time extends StatelessWidget {
  const add_stations_time({Key? key}) : super(key: key);
  static String id = 'add_stations_time';
  static final GlobalKey<FormState> _formkey3 = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    var MediaHeight = MediaQuery.of(context).size.height;
    var MediaWidth = MediaQuery.of(context).size.width;
    return ChangeNotifierProvider(
        create: (context) {},
        child: Consumer<TripAPI>(
          builder: (context, trip, child) {
            List<int> controllerTime =
                List<int>.filled(trip.Trips.length + 2, 0, growable: true);
            return Scaffold(
              floatingActionButton: FloatingActionButton(
                backgroundColor: Color(0xFF32bd8d),
                onPressed: () {
                  for (int i = 0;
                      i < trip.Trips[trip.Trips.length - 1].numStations;
                      i++) {
                    trip.Trips[trip.Trips.length - 1].time_stations
                        .add(controllerTime[i]);
                  }
                  if (_formkey3.currentState!.validate()) {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new add_buses_details()));
                  }
                },
                child: Icon(Icons.check),
              ),
              appBar:
                NewGradientAppBar(
                    leading: IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: Icon(Icons.keyboard_arrow_left),
                    ),
                    centerTitle: true,
                    //backgroundColor: Color(0xFF00D3A0),
                    elevation: 0,
                    title: Text(
                      'Time Between Stations',
                      style: GoogleFonts.montserrat(fontWeight: FontWeight.w500),
                    ),
                    gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: [
                          Color(0xFF2c8e82),
                          Color(0xFF42dc8f),
                        ])),
              backgroundColor: Colors.white,
              body: Form(
                key: _formkey3,
                child: Column(
                  children: [
                    SizedBox(
                      height: MediaHeight * 0.02,
                    ),
                    Expanded(
                      child: ListView.builder(
                          itemCount:
                              trip.Trips[trip.Trips.length - 1].numStations,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: EdgeInsets.only(
                                  top: MediaHeight * 0.01,
                                  left: MediaWidth * 0.07,
                                  right: MediaWidth * 0.07),
                              child: Column(
                                children: [
                                  TextFormField(
                                    decoration: InputDecoration(
                                      focusedBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(20)),
                                          borderSide: BorderSide(
                                              width: 2,
                                              color: Color(0xff33C58E))),
                                      labelText:
                                          'Time Required Between Stations',
                                      labelStyle:
                                          TextStyle(color: Colors.black),
                                      prefixIcon: Icon(
                                        Icons.location_on,
                                        color: Color(0xff33C58E),
                                      ),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20)),
                                      ),
                                    ),
                                    onChanged: (value) {
                                      controllerTime[index] = int.parse(value);
                                      print(controllerTime[index]);
                                    },
                                    validator: (value) {
                                      if (value!.isEmpty)
                                        return 'Field Required';
                                      else
                                        return null;
                                    },
                                    keyboardType: TextInputType.number,
                                  ),
                                  SizedBox(
                                    height: MediaHeight * 0.03,
                                  ),
                                ],
                              ),
                            );
                          }),
                    ),
                  ],
                ),
              ),
            );
          },
        ));
  }
}
