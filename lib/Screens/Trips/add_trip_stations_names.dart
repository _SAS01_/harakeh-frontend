import 'package:admin/Screens/Trips/add_stations_time.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:new_gradient_app_bar/new_gradient_app_bar.dart';
import 'package:provider/provider.dart';
import 'package:admin/Models/trips_model.dart';
import '../../Models/station_model.dart';

List<String> controller =
    List<String>.filled(TripAPI().Trips.length + 1, '', growable: true);

class add_trip_details extends StatefulWidget {
  const add_trip_details({Key? key}) : super(key: key);
  static String id = 'add_trip_details';
  static final GlobalKey<FormState> _formkey2 = GlobalKey<FormState>();

  @override
  State<add_trip_details> createState() => _add_trip_detailsState();
}

class _add_trip_detailsState extends State<add_trip_details> {
  final List<String> places = [];
  var x = 0;

  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    var MediaHeight = MediaQuery.of(context).size.height;
    var MediaWidth = MediaQuery.of(context).size.width;

    return ChangeNotifierProvider(
        create: (context) {},
        child: Consumer<TripAPI>(
          builder: (context, trip, child) {
            String selecteditem = "Choose station";
            return Scaffold(
              floatingActionButton: FloatingActionButton(
                backgroundColor: Color(0xFF32bd8d),
                onPressed: () {
                  for (int i = 0;
                      i < trip.Trips[trip.Trips.length - 1].numStations;
                      i++) {
                    trip.Trips[trip.Trips.length - 1].stations
                        .add(controller[i]);
                  }
                  // for(int i=0;i<trip.Trips[trip.Trips.length - 1].numStations;i++)
                  //   {
                  //     print(trip.Trips[trip.Trips.length - 1].stations[i]);
                  // }
                  // for(int i=0;i<controller.length;i++)
                  // {
                  //   print(controller[i]);
                  // }
                  final isValidForm =
                      add_trip_details._formkey2.currentState!.validate();
                  if (isValidForm) {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new add_stations_time()));
                  }
                },
                child: Icon(Icons.check),
              ),
              appBar: NewGradientAppBar(
                  leading: IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: Icon(Icons.keyboard_arrow_left),
                  ),
                  centerTitle: true,
                  //backgroundColor: Color(0xFF00D3A0),
                  elevation: 0,
                  title: Text(
                    'Add Trip Stations',
                    style: GoogleFonts.montserrat(fontWeight: FontWeight.w500),
                  ),
                  gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: [
                        Color(0xFF2c8e82),
                        Color(0xFF42dc8f),
                      ])),
              backgroundColor: Colors.white,
              body: Form(
                key: add_trip_details._formkey2,
                child: Column(
                  children: [
                    Consumer<StationModel>(builder: (context, station, child) {
                      if (x == 0) {
                        for (int i = 0; i < station.StationList.length; i++) {
                          places.add(station.StationList[i].name);
                        }
                        x++;
                      }

                      return Expanded(
                        child: ListView.builder(
                            itemCount:
                                trip.Trips[trip.Trips.length - 1].numStations,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding: EdgeInsets.only(
                                    top: MediaHeight * 0.01,
                                    left: MediaWidth * 0.07,
                                    right: MediaWidth * 0.07),
                                child: Column(
                                  children: [
                                    SizedBox(
                                      height: MediaHeight * 0.02,
                                    ),
                                    Consumer<StationModel>(
                                        builder: (context, station, builder) {
                                      return Container(
                                        height: MediaHeight * 0.06,
                                        child: DropdownSearch<String>(
                                          items: places,
                                          popupProps:
                                              PopupPropsMultiSelection.dialog(
                                            showSelectedItems: true,
                                            showSearchBox: true,
                                          ),
                                          validator: (value) =>
                                              value == "Choose station"
                                                  ? 'field required'
                                                  : null,
                                          onChanged: (value) {
                                            selecteditem = value!;
                                            for (int i = 0;
                                                i < station.StationList.length;
                                                i++) {
                                              if (value ==
                                                  station.StationList[i].name) {
                                                controller[index] =
                                                    station.StationList[i].name;
                                                print('');
                                                break;
                                              }
                                            }
                                          },
                                          selectedItem: selecteditem,
                                        ),
                                      );
                                    })
                                  ],
                                ),
                              );
                            }),
                      );
                    }),
                    SizedBox(
                      height: MediaHeight * 0.03,
                    ),
                    SizedBox(
                      height: MediaHeight * 0.03,
                    ),
                  ],
                ),
              ),
            );
          },
        ));
  }
}
