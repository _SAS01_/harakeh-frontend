import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:admin/Components/my_drawer.dart';
import 'package:admin/Screens/Trips/trip_details.dart';
import 'package:admin/Models/trips_model.dart';
import '../../Components/search_bar.dart';

class Trips extends StatefulWidget {
  static String id = 'trips';

  @override
  State<Trips> createState() => TripsState();
}

class TripsState extends State<Trips> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Trips',
          style: GoogleFonts.montserrat(fontWeight: FontWeight.w500),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: TripsBody(),
    );
  }
}

class TripsBody extends StatefulWidget {
  @override
  State<TripsBody> createState() => _TripsBodyState();
}

class _TripsBodyState extends State<TripsBody> {
  late List<Trip> trips;
  String query = '';

  @override
  void initState() {
    super.initState();

    trips = Provider.of<TripAPI>(context, listen: false).Trips;
  }

  @override
  Widget build(BuildContext context1) {
    return Column(
      children: [
        Stack(
          children: [
            Container(
              margin: EdgeInsets.only(
                  bottom: MediaQuery.of(context).size.height / 22),
              height: MediaQuery.of(context).size.height / 6,
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25)),
                  gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      stops: [
                        0.02,
                        0.5,
                      ],
                      colors: [
                        Color(0xFF2c8e82),
                        Color(0xFF42dc8f),
                      ])),
            ),
            Positioned(
              child: SearchBar(),
              bottom: 0,
              left: 0,
              right: 0,
            ),
          ],
        ),
        SizedBox(
          height: 15,
        ),
        Expanded(
          //Listview
          child: MediaQuery.removePadding(
            context: context,
            removeTop: true,
            child: ListView.builder(
              //shrinkWrap: true,
              itemCount: trips.length,
              itemBuilder: (context, index) {
                final trip = trips[index];
                return buildTrip(trip, context1, index);
              },
            ),
          ),
        ),
      ],
    );
  }

  Widget SearchBar() => SearchWidget(
    text: query,
    hintText: 'Search for a trip..',
    onChanged: searchTrip,
  );

  void searchTrip(String query) {
    final trips =
    Provider.of<TripAPI>(context, listen: false).Trips.where((trip) {
      final nameLower = trip.name.toLowerCase();
      final searchLower = query.toLowerCase();

      return nameLower.contains(searchLower);
    }).toList();

    setState(() {
      this.query = query;
      this.trips = trips;
    });
  }

  Widget buildTrip(Trip trip, BuildContext context, int index) {
    return ListTile(
        leading: SvgPicture.asset(
          'assets/route.svg',
          fit: BoxFit.cover,
          width: 50,
          height: 50,
        ),
        title: Text(
          trip.name,
          style: GoogleFonts.montserrat(fontWeight: FontWeight.w500),
        ),
        subtitle: Text('Click to edit'),
        onTap: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return TripDetails(
              index: index,
              trip: trip,
            );
          }));
        });
  }
}
