import 'package:admin/Models/bus_model.dart';
import 'package:admin/Models/station_model.dart';
import 'package:admin/Screens/Trips/trips.dart';
import 'package:admin/Screens/Trips/trips.dart';
import 'package:admin/service/http.dart';
import 'package:admin/service/signin_api.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import '../../Components/constants.dart';
import '../../Models/trips_model.dart';
import '../HomePage/newwelcom.dart';
late List<String>places =[];
late List<String>buses =[];
late String currentTrip;
Trip tripObject = new Trip(name: '', time_stations: [], buses: [], numBuses: 0, numStations: 0, stations: []);
class TripDetails extends StatelessWidget {
  final int index;
  final Trip trip;
  static String id = 'trip_details';

  const TripDetails({required this.trip, required this.index});
  @override
  Widget build(BuildContext context) {
    void showError(String message) {
      showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text('Something went wrong'),
            content: Text(message),
            actions: [
              MaterialButton(
                  child: Text(
                    'Okay',
                    style: TextStyle(color: Colors.white),
                  ),
                  color: mainColor,
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pop();
                  }),
            ],
          ));
    }

    void showSuccess(String message) {
      showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text('Great Work!'),
            content: Text(message),
            actions: [
              MaterialButton(
                  child: Text(
                    'Okay',
                    style: TextStyle(color: Colors.white),
                  ),
                  color: mainColor,
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pushReplacement(MaterialPageRoute(builder: (context)=>Welcome()));

                  }),
            ],
          ));
    }
    Http http = new Http();
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              stops: [
                0.1,
                0.28,
              ],
              colors: [
                Color(0xFF2c8e82),
                Color(0xFF42dc8f),
              ])),
      child:
      Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: () async{
            print('${currentTrip}');
            List<Trip> trip;
            trip=Provider.of<TripAPI>(context, listen: false).Trips;
            var x = await http.editTrip(name: tripObject.name, stations: tripObject.stations, times: tripObject.time_stations, buses:tripObject.buses,currentTrip: currentTrip ,token: Provider.of<SignInProvider>(context, listen: false).token);
            if(x=='Done Successfully')
            {
              showSuccess(x);
            }
            else
            {
              showError(x);
            }
          },
          child: Icon(Icons.check),
          backgroundColor: Color(0xFF32bd8d),
        ),

        backgroundColor: Colors.transparent,
        // extendBodyBehindAppBar: true,
        appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Trip Details',
          style: GoogleFonts.montserrat(fontWeight: FontWeight.w500),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
        actions: [
          IconButton(onPressed: ()async{
           var x = await http.deleteTrip(currentTrip: currentTrip ,token: Provider.of<SignInProvider>(context, listen: false).token);
            if(x=='Done Successfully')
            {
              showSuccess(x);
            }
            else
            {
              showError(x);
            }
          }, icon: Icon(
            Icons.delete,
          ))
        ],
      ),
        body: Column(
          children: [
            SizedBox(
              height: 50,
            ),
            Expanded(
              child: Stack(
                children: [
                  Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30)),
                      )),
                  TripDetailsBody(trip, index),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class TripDetailsBody extends StatefulWidget {
  final int indexTop;
  final Trip trip;

  TripDetailsBody(this.trip, this.indexTop);
  @override
  State<TripDetailsBody> createState() => _TripDetailsBodyState(trip, indexTop);
}

class _TripDetailsBodyState extends State<TripDetailsBody> {
  late int indexTop = 0;
  late List<Trip> trips;
  late List<Station> temp;
  late List<BusModel> temp2;

  final Trip trip;

  _TripDetailsBodyState(this.trip, this.indexTop);
  @override
  void initState() {
    super.initState();

    trips = Provider.of<TripAPI>(context, listen: false).Trips;
    tripObject.name = trips[indexTop].name;
    tripObject.time_stations = trips[indexTop].time_stations;
    tripObject.stations = trips[indexTop].stations;
    tripObject.buses = trips[indexTop].buses;
    temp =  Provider.of<StationModel>(context, listen: false).StationList;
    List<dynamic>temp2 =  Provider.of<TripAPI>(context, listen: false).Trips[0].stations;
    for(int i=0;i<temp2.length;i++)
      {
        print('${temp2[i]}');
    }
    for(int i=0;i<temp.length;i++)
      {
        places.add(temp[i].name);
    }
    temp2 =  Provider.of<BusAPI>(context, listen: false).availbleBuses;
    for(int i=0;i<temp2.length;i++)
    {
      buses.add(temp2[i].id.toString());
    }
    currentTrip = trips[indexTop].name;
  }

  @override
  Widget build(BuildContext context) {
    var MediaWidth= MediaQuery.of(context).size.width;
    var MediaHeight= MediaQuery.of(context).size.height;

    dynamic selecteditem = "Choose Bus Number";

    return Padding(
      padding: EdgeInsets.only(
          top: MediaQuery.of(context).size.height * 0.01,
          left: MediaQuery.of(context).size.height * 0.03,
          right: MediaQuery.of(context).size.width * 0.07),
      child: Column(
        children: [
          SizedBox(
            height: MediaHeight * 0.02,
          ),


          Padding(
            padding:EdgeInsets.only(
                top: MediaHeight * 0.01,
                left: MediaWidth * 0.1,
                right: MediaWidth * 0.1),
            child: TextField(
              decoration: InputDecoration(
                focusedBorder: OutlineInputBorder(
                    borderRadius:
                    BorderRadius.all(Radius.circular(20)),
                    borderSide: BorderSide(
                        width: 2, color: Color(0xff33C58E))),
                hintText: 'Edit ${trip.name} name ',
                labelStyle: TextStyle(color: Colors.black),
                prefixIcon: Icon(
                  Icons.account_circle,
                  color: Color(0xff33C58E),
                ),
                border: OutlineInputBorder(
                  borderRadius:
                  BorderRadius.all(Radius.circular(20)),
                ),
              ),
              onChanged: (value)
              {
                    tripObject.name = value;
              },

            ),
          ),

          SizedBox(height: MediaHeight *0.03,),


          Container(
            height: MediaHeight *0.065,
            width: MediaWidth *0.66,
            decoration: BoxDecoration(
                color: Color(0xFF32bd8d),
                borderRadius: BorderRadius.all(Radius.circular(20))
            ),
            child:Consumer<TripAPI>(builder: (context , tripp , child){

              return TextButton.icon(onPressed: (){
                showModalBottomSheet(context: context,
                    shape:RoundedRectangleBorder(

                        borderRadius: BorderRadius.vertical(
                          top: Radius.circular(40),
                        )
                    ),
                    builder: (context)=> Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(topRight: Radius.circular(40),topLeft: Radius.circular(40)),
                          border: Border.all(
                            color: Color(0xFF32bd8d),
                            width: 10
                          )
                      ),
                      child: Stack(
                        children: [
                          ListView.builder(itemCount: trip.stations.length,itemBuilder: (context,index){
                            return Padding(
                              padding: EdgeInsets.only(top: MediaHeight * 0.01, left: MediaWidth * 0.07, right: MediaWidth * 0.07),
                              child: Column(
                                children: [
                                  SizedBox(
                                    height: MediaHeight *0.07,
                                  ),
                                  Stack(
                                    children: [
                                      DropdownButtonHideUnderline(
                                        child:DropdownSearch<String>(
                                          items: places,
                                          popupProps: PopupPropsMultiSelection.dialog(
                                            showSelectedItems: true,
                                            showSearchBox: true,
                                          ),
                                          onChanged: (value) {
                                            selecteditem = value!;
                                            for(int i=0;i<places.length;i++)
                                            {
                                              if(value == places[i])
                                              {
                                                tripObject.stations[index]= value;
                                                print(value);
                                                break;
                                              }
                                            }

                                          },
                                          selectedItem: trip.stations[index],
                                        ),
                                      ),
                                      Padding(
                                        padding:  EdgeInsets.only(left: MediaWidth * 0.7),
                                        child: IconButton(onPressed: (){tripp.deleteStation(indexTop, index);
                                        }, icon: Icon(Icons.delete , color: Color(0xFF32bd8d),)),
                                      ),
                                    ],
                                  ),

                                ],
                              ),
                            );
                          }),
                          Padding(
                            padding: EdgeInsets.only(left: MediaWidth *0.33 , top: MediaHeight * 0.01),
                            child:                 Container(
                              width: MediaWidth * 0.3,
                              height: MediaHeight * 0.05,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(20)),
                                color: Color(0xFF32bd8d),
                              ),
                              child: RaisedButton(
                                  color: Color(0xff33C58E),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20)),
                                  child: Text(
                                    'Add Station',
                                    style: GoogleFonts.montserrat(fontSize: 14, color: Colors.white , fontWeight: FontWeight.w500),
                                  ),
                                  onPressed: ()  {tripp.addStation(indexTop);}
                              ),
                            ),

                          )

                        ],
                      ),
                    ));
              },   icon:Icon(Icons.location_on , color: Colors.white,),
                  label: Text('Edit Trip Stations' , style: TextStyle(
                      color: Colors.white,
                      fontSize: 18
                  ),));

            })


          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.03,
          ),
          Container(
            height: MediaHeight *0.065,
            width: MediaWidth *0.66,
            decoration: BoxDecoration(
                color: Color(0xFF32bd8d),
                borderRadius: BorderRadius.all(Radius.circular(20))
            ),
            child: Consumer<TripAPI>(builder:(context , tripp, child){
              return TextButton.icon(onPressed: (){
                showModalBottomSheet(context: context,
                    shape:RoundedRectangleBorder(

                        borderRadius: BorderRadius.vertical(
                          top: Radius.circular(40),
                        )
                    ),builder: (context)=> Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(topRight: Radius.circular(40),topLeft: Radius.circular(40)),
                      border: Border.all(
                          color: Color(0xFF32bd8d),
                          width: 10
                      )
                  ),
                  child: Center(
                    child: ListView.builder(
                        itemCount: trip.time_stations.length,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: EdgeInsets.only(
                                top: MediaHeight * 0.01,
                                left: MediaWidth * 0.07,
                                right: MediaWidth * 0.07),
                            child: Column(
                              children: [
                                TextFormField(
                                  decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius:
                                        BorderRadius.all(Radius.circular(20)),
                                        borderSide: BorderSide(
                                            width: 2, color: Color(0xff33C58E))),
                                    hintText: 'Time Station ${index+1} and ${index+2} is ${trip.time_stations[index]}',
                                    labelStyle: TextStyle(color: Colors.black),
                                    prefixIcon: Icon(
                                      Icons.location_on,
                                      color: Color(0xff33C58E),
                                    ),
                                    border: OutlineInputBorder(
                                      borderRadius:
                                      BorderRadius.all(Radius.circular(20)),
                                    ),
                                  ),
                                  onChanged: (value) {
                                   tripObject.time_stations[index]=value;
                                  },

                                  keyboardType: TextInputType.number,
                                ),
                                SizedBox(
                                  height: MediaHeight * 0.03,
                                ),
                              ],
                            ),
                          );
                        }),

                  ),
                ));

              },
                  icon:Icon(Icons.alarm , color: Colors.white,),
                  label: Text('Edit Stations Time' , style: TextStyle(
                      color: Colors.white,
                      fontSize: 18
                  ),));
              //child: Text('Edit Stations Time'));
            }),
          ),
          SizedBox(height: MediaHeight *0.03,),

          Container(
            height: MediaHeight *0.065,
            width: MediaWidth *0.66,
            decoration: BoxDecoration(
                color: Color(0xFF32bd8d),
                borderRadius: BorderRadius.all(Radius.circular(20))
            ),
            child:           Consumer<TripAPI>(builder: (context , tripp, child){
              return           TextButton.icon(onPressed: (){
                showModalBottomSheet(context: context,
                    shape:RoundedRectangleBorder(

                        borderRadius: BorderRadius.vertical(
                          top: Radius.circular(40),
                        )
                    ),builder: (context)=> Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(topRight: Radius.circular(40),topLeft: Radius.circular(40)),
                      border: Border.all(
                          color: Color(0xFF32bd8d),
                          width: 10
                      )
                  ),
                  child: Stack(
                      children: [
                        Center(
                          child: ListView.builder(itemCount: trip.buses.length,itemBuilder: (context,index){
                            return Padding(
                              padding: EdgeInsets.only(top: MediaHeight * 0.01, left: MediaWidth * 0.07, right: MediaWidth * 0.07),
                              child: Column(
                                children: [
                                  SizedBox(height: MediaHeight *0.07,),

                                  Stack(
                                    children: [
                                      DropdownButtonHideUnderline(
                                        child: DropdownSearch<String>(
                                          items: buses,
                                          popupProps: PopupPropsMultiSelection.menu(
                                            showSelectedItems: true,
                                            showSearchBox: true,
                                          ),

                                          onChanged: (value) {
                                            selecteditem = value!;
                                            tripObject.buses[index]=value;
                                          },
                                          selectedItem: '${trip.buses[index]}',
                                        ),
                                      ),
                                      Padding(
                                        padding:  EdgeInsets.only(left:  MediaWidth * 0.7),
                                        child: IconButton(onPressed: (){tripp.deleteBus(indexTop , index);
                                        }, icon: Icon(Icons.delete , color: Color(0xFF32bd8d))),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            );
                          }),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: MediaWidth *0.33 , top: MediaHeight * 0.01),
                          child:                 Container(
                            width: MediaWidth * 0.3,
                            height: MediaHeight * 0.05,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(20)),
                              color: Color(0xFF32bd8d),
                            ),
                            child: RaisedButton(
                                color: Color(0xff33C58E),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20)),
                                child: Text(
                                  'Add Bus',
                                  style: GoogleFonts.montserrat(fontSize: 14, color: Colors.white , fontWeight: FontWeight.w500),
                                ),
                                onPressed: ()  {tripp.addBus(indexTop);}
                            ),
                          ),

                        )
                      ]
                  ),
                ));
              },
                  icon:Icon(Icons.bus_alert , color: Colors.white,),
                  label: Text('Edit Trip Buses' , style: TextStyle(
                      color: Colors.white,
                      fontSize: 18
                  ),));
            }),

          ),
        ],
      ),
    );
  }
}

Widget buildTrip(Trip trip, BuildContext context, int index) {
  print('');
  String buses = trip.buses[index];
  return Column(
    children: [
      ClipRRect(
          child: Image.asset('assets/bus.png',
              height: MediaQuery.of(context).size.height / 10,
              width: MediaQuery.of(context).size.height / 10)),
      SizedBox(
        height: 8,
      ),
      Text('Bus $buses',
          style: GoogleFonts.montserrat(fontWeight: FontWeight.w500))
    ],
  );

}
