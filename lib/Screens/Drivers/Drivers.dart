import 'package:admin/Components/constants.dart';
import 'package:admin/Models/driver_model.dart';
import 'package:admin/Screens/Drivers/DriverDetails.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:admin/Components/my_drawer.dart';
import 'package:admin/Screens/Trips/trip_details.dart';
import 'package:admin/Models/trips_model.dart';
import '../../../Components/search_bar.dart';

class Drivers extends StatefulWidget {
  static String id = 'Drivers';

  @override
  State<Drivers> createState() => DriversState();
}

class DriversState extends State<Drivers> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Drivers',
          style: GoogleFonts.montserrat(fontWeight: FontWeight.w500),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: DriversBody(),
    );
  }
}

class DriversBody extends StatefulWidget {
  @override
  State<DriversBody> createState() => _DriversBodyState();
}

class _DriversBodyState extends State<DriversBody> {
  late List<dynamic> drivers;
  String query = '';

  @override
  void initState() {
    super.initState();

    drivers = Provider.of<driverAPI>(context, listen: false).driversList;
  }

  @override
  Widget build(BuildContext context1) {
    return Column(
      children: [
        Stack(
          children: [
            Container(
              margin: EdgeInsets.only(
                  bottom: MediaQuery.of(context).size.height / 22),
              height: MediaQuery.of(context).size.height / 6,
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25)),
                  gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      stops: [
                        0.02,
                        0.5,
                      ],
                      colors: [
                        Color(0xFF2c8e82),
                        Color(0xFF42dc8f),
                      ])),
            ),
            Positioned(
              child: SearchBar(),
              bottom: 0,
              left: 0,
              right: 0,
            ),
          ],
        ),
        SizedBox(
          height: 15,
        ),
        Expanded(
          //Listview
          child: MediaQuery.removePadding(
            context: context,
            removeTop: true,
            child: ListView.builder(
              //shrinkWrap: true,
              itemCount: drivers.length,
              itemBuilder: (context, index) {
                final trip = drivers[index];
                return buildTrip(trip, context1, index);
              },
            ),
          ),
        ),
      ],
    );
  }

  Widget SearchBar() => SearchWidget(
    text: query,
    hintText: 'Search for a trip..',
    onChanged: searchTrip,
  );

  void searchTrip(String query) {
    final drivers =
    Provider.of<driverAPI>(context, listen: false).driversList.where((driver) {
      final nameLower = driver.name.toLowerCase();
      final searchLower = query.toLowerCase();

      return nameLower.contains(searchLower);
    }).toList();

    setState(() {
      this.query = query;
      this.drivers = drivers;
    });
  }

  Widget buildTrip(Driver driver, BuildContext context, int index) {
    return ListTile(
        leading: SvgPicture.asset(
          'assets/driver-svgrepo-com.svg',
          color: mainColor,
          fit: BoxFit.cover,
          width: 40,
          height: 40,
        ),
        title: Text(
          driver.name,
          style: GoogleFonts.montserrat(fontWeight: FontWeight.w500),
        ),
        subtitle: Text('Click to details'),
        onTap: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return DriverDetails(
              index: index,
              driver: driver,
            );
          }));
        });
  }
}
