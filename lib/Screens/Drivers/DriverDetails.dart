import 'package:admin/Models/bus_model.dart';

import 'package:admin/service/http.dart';
import 'package:admin/service/signin_api.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import '../../../Components/constants.dart';
import '../../Models/driver_model.dart';
import '../HomePage/newwelcom.dart';
List<String> buses = [];
List<BusModel> temp = [];
String name = '';
String nationalId='';
int busId = 0;
class DriverDetails extends StatelessWidget {
  final int index;
  final Driver driver;
  static String id = 'trip_details';

  const DriverDetails({required this.driver, required this.index});
  @override
  Widget build(BuildContext context) {

    void showError(String message) {
      showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text('Something went wrong'),
            content: Text(message),
            actions: [
              MaterialButton(
                  child: Text(
                    'Okay',
                    style: TextStyle(color: Colors.white),
                  ),
                  color: mainColor,
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pop();
                  }),
            ],
          ));
    }

    void showSuccess(String message) {
      showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text('Great Work!'),
            content: Text(message),
            actions: [
              MaterialButton(
                  child: Text(
                    'Okay',
                    style: TextStyle(color: Colors.white),
                  ),
                  color: mainColor,
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pushReplacement(MaterialPageRoute(builder: (context)=>Welcome()));

                  }),
            ],
          ));
    }
    Http http = new Http();
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              stops: [
                0.1,
                0.28,
              ],
              colors: [
                Color(0xFF2c8e82),
                Color(0xFF42dc8f),
              ])),
      child:
      Scaffold(
        floatingActionButton: FloatingActionButton(
          backgroundColor: Color(0xFF32bd8d),
          onPressed: () async{
            var x = await http.addDriver(nationalID: nationalId,  name: name,busId: busId,token: Provider.of<SignInProvider>(context, listen: false).token);
            if (x == 'Done Successfully') {
              showSuccess(x);
            } else {
              showError(x);
            }
          },
          child: Icon(Icons.check),
        ),

        backgroundColor: Colors.transparent,
        // extendBodyBehindAppBar: true,
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            'Driver Details',
            style: GoogleFonts.montserrat(fontWeight: FontWeight.w500),
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
          actions: [
            IconButton(onPressed: ()async{
              var x = await http.deleteDriver(nationalID: driver.nationalId ,token: Provider.of<SignInProvider>(context, listen: false).token);
              if(x=='Done Successfully')
              {
                showSuccess(x);
              }
              else
              {
                showError(x);
              }
            }, icon: Icon(
              Icons.delete,
            ))
          ],
        ),
        body: Column(
          children: [
            SizedBox(
              height: 50,
            ),
            Expanded(
              child: Stack(
                children: [
                  Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30)),
                      )),
                  DriverDetailsBody(driver, index),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class DriverDetailsBody extends StatefulWidget {
  final int indexTop;
  final Driver driver;

  DriverDetailsBody(this.driver, this.indexTop);
  @override
  State<DriverDetailsBody> createState() => _DriverDetailsBodyState(driver, indexTop);
}

class _DriverDetailsBodyState extends State<DriverDetailsBody> {
  late int indexTop = 0;
  late List<dynamic> drivers;

  final Driver driver;

  _DriverDetailsBodyState(this.driver, this.indexTop);
  @override
  void initState() {
    super.initState();
    drivers =  Provider.of<driverAPI>(context, listen: false).driversList;

    temp = Provider.of<BusAPI>(context, listen: false).availbleBuses;
    for (int i = 0; i < temp.length; i++) {
      buses.add(temp[i].id.toString());
    }
    name= driver.name;
    busId= driver.busId;
    nationalId = driver.nationalId;
  }

  @override
  Widget build(BuildContext context) {
    var MediaWidth= MediaQuery.of(context).size.width;
    var MediaHeight= MediaQuery.of(context).size.height;

    dynamic selecteditem = "${driver.busId}";

    return Padding(
      padding: EdgeInsets.only(
          top: MediaQuery.of(context).size.height * 0.03,
          left: MediaQuery.of(context).size.width * 0.07,
          right: MediaQuery.of(context).size.width * 0.07),
      child: Column(
        children: [

          TextField(
            decoration: InputDecoration(
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  borderSide:
                  BorderSide(width: 2, color: Color(0xff33C58E))),
              hintText: 'Driver Name is ${driver.name}',
              prefixIcon: Icon(
                Icons.drive_file_rename_outline,
                color: Color(0xff33C58E),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(20)),
              ),
            ),
            onChanged: ( value) {
              name = value;
            },
            keyboardType: TextInputType.text,
          ),
          SizedBox(
            height: MediaHeight * 0.03,
          ),
          TextField(
            decoration: InputDecoration(
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  borderSide:
                  BorderSide(width: 2, color: Color(0xff33C58E))),
              hintText: 'Driver National ID is ${driver.nationalId}',
              prefixIcon: Icon(
                Icons.drive_file_rename_outline,
                color: Color(0xff33C58E),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(20)),
              ),
            ),
            onChanged: (String value) {
              nationalId = value;
            },
            keyboardType: TextInputType.text,
          ),
          SizedBox(height: MediaHeight *0.03,),

          DropdownButtonHideUnderline(
            child: DropdownSearch<String>(
              items: buses,
              popupProps: PopupPropsMultiSelection.menu(
                showSelectedItems: true,
                showSearchBox: true,
              ),
              validator: (value) =>
              value == "Choose Bus Number"
                  ? 'field required'
                  : null,
              onChanged: (value) {
                selecteditem = value!;
                busId = int.parse(value);
              },
              selectedItem: selecteditem,
            ),
          ),

        ],
      ),
    );
  }
}

