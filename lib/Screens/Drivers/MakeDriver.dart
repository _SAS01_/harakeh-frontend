import 'package:admin/Models/bus_model.dart';
import 'package:admin/Models/driver_model.dart';
import 'package:admin/service/http.dart';
import 'package:admin/service/signin_api.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:new_gradient_app_bar/new_gradient_app_bar.dart';
import 'package:provider/provider.dart';

import '../../Components/constants.dart';
import '../HomePage/newwelcom.dart';

late String name  ;
late String nationalID;
List<String> buses = [];
List<BusModel> temp = [];

class MakeDriver extends StatefulWidget {
  static String id = 'Make Driver';
  @override
  State<MakeDriver> createState() => _MakeDriverState();
}
dynamic selecteditem = "Choose Bus Number";

class _MakeDriverState extends State<MakeDriver> {
  @override
  void initState() {
    super.initState();
    temp = Provider.of<BusAPI>(context, listen: false).BusesList;
    for (int i = 0; i < temp.length; i++) {
      buses.add(temp[i].id.toString());
    }
  }

  Widget build(BuildContext context) {
    void showError(String message) {
      showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text('Something went wrong'),
            content: Text(message),
            actions: [
              MaterialButton(
                  child: Text(
                    'Okay',
                    style: TextStyle(color: Colors.white),
                  ),
                  color: mainColor,
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pop();
                  }),
            ],
          ));
    }

    void showSuccess(String message) {
      showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text('Great Work!'),
            content: Text(message),
            actions: [
              MaterialButton(
                  child: Text(
                    'Okay',
                    style: TextStyle(color: Colors.white),
                  ),
                  color: mainColor,
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pushReplacement(MaterialPageRoute(builder: (context)=>Welcome()));

                  }),
            ],
          ));
    }

    var MediaHeight = MediaQuery.of(context).size.height;
    var MediaWidth = MediaQuery.of(context).size.width;
    Http http = new Http();
    return ChangeNotifierProvider(create: (context) {
      return driverAPI(
      );
    }, child: Consumer<driverAPI>(
      builder: (context, driver, child) {
        return Scaffold(
          appBar: NewGradientAppBar(
              leading: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(Icons.keyboard_arrow_left),
              ),
              centerTitle: true,
              //backgroundColor: Color(0xFF00D3A0),
              elevation: 0,
              title: Text(
                'Add Driver',
                style: GoogleFonts.montserrat(fontWeight: FontWeight.w500),
              ),
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                    Color(0xFF2c8e82),
                    Color(0xFF42dc8f),
                  ])),
          floatingActionButton: FloatingActionButton(
            backgroundColor: Color(0xFF32bd8d),
            onPressed: () async{
              var x = await http.addDriver(nationalID: nationalID,  name: name,busId: int.parse(selecteditem),token: Provider.of<SignInProvider>(context, listen: false).token);
              if (x == 'Done Successfully') {
                showSuccess(x);
              } else {
                showError(x);
              }
            },
            child: Icon(Icons.check),
          ),
          backgroundColor: Colors.white,
          body: Container(
            color: Colors.white,
            child: Padding(
              padding: EdgeInsets.only(
                  top: MediaHeight * 0.1,
                  left: MediaWidth * 0.07,
                  right: MediaWidth * 0.07),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  TextField(
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          borderSide:
                          BorderSide(width: 2, color: Color(0xff33C58E))),
                      labelText: 'Diver Name',
                      labelStyle: TextStyle(color: Colors.black),
                      prefixIcon: Icon(
                        Icons.drive_file_rename_outline,
                        color: Color(0xff33C58E),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                      ),
                    ),
                    onChanged: ( value) {
                      name = value;
                    },
                    keyboardType: TextInputType.text,
                  ),
                  SizedBox(
                    height: MediaHeight * 0.03,
                  ),
                  TextField(
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          borderSide:
                          BorderSide(width: 2, color: Color(0xff33C58E))),
                      labelText: 'Diver National ID',
                      labelStyle: TextStyle(color: Colors.black),
                      prefixIcon: Icon(
                        Icons.drive_file_rename_outline,
                        color: Color(0xff33C58E),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                      ),
                    ),
                    onChanged: (String value) {
                      nationalID = value;
                    },
                    keyboardType: TextInputType.text,
                  ),
                  SizedBox(height: MediaHeight *0.03,),

                  DropdownButtonHideUnderline(
                    child: DropdownSearch<String>(
                      items: buses,
                      popupProps: PopupPropsMultiSelection.menu(
                        showSelectedItems: true,
                        showSearchBox: true,
                      ),
                      validator: (value) =>
                      value == "Choose Bus Number"
                          ? 'field required'
                          : null,
                      onChanged: (value) {
                        selecteditem = value!;
                        },
                      selectedItem: selecteditem,
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    ));
  }
}
