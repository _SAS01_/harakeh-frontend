import 'package:admin/Models/complaints_model.dart';
import 'package:admin/Models/driver_model.dart';
import 'package:admin/Models/news_model.dart';
import 'package:admin/Models/station_model.dart';
import 'package:admin/Models/trips_model.dart';
import 'package:admin/Screens/Buses/add_bus.dart';
import 'package:admin/Screens/Drivers/Drivers.dart';
import 'package:admin/Screens/Loading_indicator.dart';
import 'package:admin/Screens/Drivers/MakeDriver.dart';
import 'package:admin/Screens/SplashScreen.dart';
import 'package:admin/Screens/Trips/add_stations_time.dart';
import 'package:admin/Screens/Stations/add_station.dart';
import 'package:admin/Screens/Stations/stations.dart';
import 'package:admin/Screens/Trips/trips.dart';
import 'package:admin/Screens/news.dart';
import 'package:admin/Screens/sign_in.dart';
import 'package:admin/Screens/HomePage/newwelcom.dart';
import 'package:admin/service/signin_api.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:admin/Screens/Trips/add_buses_details.dart';
import 'package:admin/Screens/Trips/add_trip_stations_names.dart';
import 'package:admin/Screens/Trips/add_trip.dart';
import 'package:admin/Models/bus_model.dart';

import 'Screens/complaints.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (context) => TripAPI(),
          ),
          ChangeNotifierProvider(
            create: (context) => SignInProvider(),
          ),
          ChangeNotifierProvider(
            create: (context) => newsAPI(),
          ),
          ChangeNotifierProvider(
            create: (context) => ComplaintsAPI(),
          ),
          ChangeNotifierProvider(
            create: (context) => BusAPI(),
          ),
          ChangeNotifierProvider(
            create: (context) => driverAPI(),
          ),
          ChangeNotifierProvider(
            create: (context) => StationModel(),
          ),
        ],
        child: Consumer<SignInProvider>(
            builder: (context, auth, child) => MaterialApp(
                  key: Key('auth_${auth.isAuth}'),
                  debugShowCheckedModeBanner: false,
                  home: auth.isAuth ? Welcome() : SplashScreen(),
                  routes: {
                    Drivers.id:(context)=>Drivers(),
                    SplashScreen.id: (context) => SplashScreen(),
                    Welcome.id:(context)=>Welcome(),
                    SignIn.id: (context) => SignIn(),
                    LoadingScreenIndicator.id: (context) =>
                        LoadingScreenIndicator(),
                    MakeDriver.id:(context)=>MakeDriver(),
                    Complaints.id: (context) => Complaints(),
                    NewsScreen.id: (context) => NewsScreen(),
                    Stations.id: (context) => Stations(),
                    Trips.id: (contest) => Trips(),
                    add_station.id: (context) => add_station(),
                    add_bus.id: (context) => add_bus(),
                    add_stations_time.id: (context) => add_stations_time(),
                    add_buses_details.id: (context) => add_buses_details(),
                    add_trip_details.id: (context) => add_trip_details(),
                    add_trip.id: (context) => add_trip(),
                  },
                )));
  }
}

//here we go
//Tony was here
