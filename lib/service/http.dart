import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class Http with ChangeNotifier {
  String ip = 'https://harakeh-backend.herokuapp.com/api';
  Future add_station(
      {required String name,
      required double lat,
      required double lng,
      required String token}) async {
    Map<String, dynamic> body = {
      "name": name,
      "lat": lat,
      "lng": lng,
    };
    Uri url = Uri.parse('$ip/stations');
    print('ss');
    print('$name  $lat $lng');
    http.Response response = await http.post(url,
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'X-ACCESS-TOKEN': token
        },
        body: jsonEncode(body));

    int status = response.statusCode;
    notifyListeners();
    final responseData = jsonDecode(response.body);
    String answer = responseData['message'];
    if(status ==200 || status ==201)
      {
        return 'Done Successfully';
      }
    else
      {
        return answer;
      }
    // var info = jsonDecode(response.body);
  }

  Future editStaion(
      {required String name,
      required double lat,
      required double lng,
      required String currentName,
      required String token}) async {
    Map<String, dynamic> body = {
      "name": name,
      "lat": lat,
      "lng": lng,
    };
    Uri url = Uri.parse('$ip/stations/editByName/$currentName');
    print('$currentName  $lat $lng');
    http.Response response = await http.put(url,
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'X-ACCESS-TOKEN': token
        },
        body: jsonEncode(body));

    int status = response.statusCode;
    print(status);
    print(currentName + name);
    notifyListeners();
    final responseData = jsonDecode(response.body);
    String answer = responseData['message'];
    if(status ==200 || status ==201)
    {
      return 'Done Successfully';
    }
    else
    {
      return answer;
    }
    // var info = jsonDecode(response.body);

  }

  Future deleteStation(
      {required String currentStation, required String token}) async {
    print(currentStation);
    Map<String, dynamic> toJson() => {};
    Uri url = Uri.parse('$ip/stations/$currentStation');
    print('1');
    http.Response response = await http.delete(url,
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'X-ACCESS-TOKEN':
token        },
        body: jsonEncode(toJson()));

    int status = response.statusCode;
    var x = response.body;
    notifyListeners();
    print(x);

    // var info = jsonDecode(response.body);
    final responseData = jsonDecode(response.body);
    String answer = responseData['message'];
    if(status ==200 || status ==201)
    {
      return 'Done Successfully';
    }
    else
    {
      return answer;
    }
  }

  Future deleteTrip(
      {required String currentTrip, required String token}) async {
    print(currentTrip);
    Map<String, dynamic> toJson() => {};
    Uri url = Uri.parse('$ip/routes/$currentTrip');
    print('1');
    http.Response response = await http.delete(url,
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'X-ACCESS-TOKEN': token,
        },
        body: jsonEncode(toJson()));

    int status = response.statusCode;
    var x = response.body;
    notifyListeners();
    print(status);
    // var info = jsonDecode(response.body);
    final responseData = jsonDecode(response.body);
    String answer = responseData['message'];
    if(status ==200 || status ==201)
    {
      return 'Done Successfully';
    }
    else
    {
      return answer;
    }
  }

  Future add_trip(
      {required String name,
      required List<dynamic> stations,
      required List<dynamic> times,
      required List<dynamic> buses,
      required String token}) async {
    Map<String, dynamic> toJson() {
      final Map<String, dynamic> data = new Map<String, dynamic>();
      data['name'] = name;
      data['stations'] = stations;
      data['time'] = times;
      data['BusesId'] = buses;
      return data;
    }

    print('$name + $stations + $times + $buses ');
    Uri url = Uri.parse('$ip/routes');
    http.Response response = await http.post(url,
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'X-ACCESS-TOKEN':
token        },
        body: jsonEncode(toJson()));
    notifyListeners();
    int status = response.statusCode;
    print(status);
    // var info = jsonDecode(response.body);
    final responseData = jsonDecode(response.body);
    String answer = responseData['message'];
    if(status ==200 || status ==201)
    {
      return 'Done Successfully';
    }
    else
    {
      return answer;
    }
  }

  Future editTrip(
      {required String name,
      required List<dynamic> stations,
      required List<dynamic> times,
      required List<dynamic> buses,
      required String currentTrip,
      required String token}) async {
    print('${name} + $stations + $times + $buses + $currentTrip');
    Map<String, dynamic> toJson() {
      final Map<String, dynamic> data = new Map<String, dynamic>();
      data['name'] = name;
      data['stations'] = stations;
      data['time'] = times;
      data['BusesId'] = buses;
      return data;
    }

    Uri url = Uri.parse('$ip/routes/$currentTrip');
    http.Response response = await http.put(url,
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'X-ACCESS-TOKEN':
token        },
        body: jsonEncode(toJson()));

    int status = response.statusCode;
    var rr = response.body;
    notifyListeners();
    print('${rr}');
    // var info = jsonDecode(response.body);
    final responseData = jsonDecode(response.body);
    String answer = responseData['message'];
    if(status ==200 || status ==201)
    {
      return 'Done Successfully';
    }
    else
    {
      return answer;
    }
  }

  Future add_bus(
      {required int id,
      required String startingStation,
      required String route,
      required List<String> workingDays,
      required int stHour,
      required int enHour,
      required bool free,
      required String token}) async {
    print('$id  + $startingStation + $route + $stHour + $enHour + $free');
    Map<String, dynamic> toJson() => {
          "id": id,
          "route": route,
          "workingDays": List<dynamic>.from(workingDays.map((x) => x)),
          "stHour": stHour,
          "enHour": enHour,
          "free": free,
          "startingStation": startingStation,
        };
    print(startingStation);
    Uri url = Uri.parse('$ip/buses');
    http.Response response = await http.post(url,
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'X-ACCESS-TOKEN':
token        },
        body: jsonEncode(toJson()));

    String pp = response.body;
    int status = response.statusCode;
    print('${status} + ${response.body}');
    notifyListeners();
    // var info = jsonDecode(response.body);
    final responseData = jsonDecode(response.body);
    String answer = responseData['message'];
    if(status ==200 || status ==201)
    {
      return 'Done Successfully';
    }
    else
    {
      return answer;
    }
  }

  Future editBus(
      {required int id,
      required String startingStation,
      required String route,
      required List<String> workingDays,
      required int stHour,
      required int enHour,
      required bool free,
      required int currentId,
      required String token}) async {
    Map<String, dynamic> toJson() => {
          "id": id,
          "route": route,
          "workingDays": List<dynamic>.from(workingDays.map((x) => x)),
          "stHour": stHour,
          "enHour": enHour,
          "free": free,
          "startingStation": startingStation,
        };
    print(
        '$currentId + $id + $startingStation + $route + $workingDays + $stHour + $enHour + $free');
    Uri url = Uri.parse('$ip/buses/$currentId');
    http.Response response = await http.put(url,
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'X-ACCESS-TOKEN':
token        },
        body: jsonEncode(toJson()));

    int status = response.statusCode;
    var message = response.body;
    notifyListeners();
    final responseData = jsonDecode(response.body);
    String answer = responseData['message'];
    print(answer);
    if(status ==200 || status ==201)
    {
      return 'Done Successfully';
    }
    else
    {
      return answer;
    }
  }

  Future deleteBus({required int id, required String token}) async {
    print(id);
    Map<String, dynamic> toJson() => {};
    Uri url = Uri.parse('$ip/buses/$id');
    print('1');
    http.Response response = await http.delete(url,
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'X-ACCESS-TOKEN':
token        },
        body: jsonEncode(toJson()));

    int status = response.statusCode;
    var x = response.body;
    print(x);
    notifyListeners();
    // var info = jsonDecode(response.body);
    final responseData = jsonDecode(response.body);
    String answer = responseData['message'];
    if(status ==200 || status ==201)
    {
      return 'Done Successfully';
    }
    else
    {
      return answer;
    }
  }

  Future addNews(
      {required String title,
      required String newNews,
      required String token}) async {
    print('$title + $newNews');
    Map<String, dynamic> toJson() => {
          "title": title,
          "content": newNews,
        };
    print('2');
    Uri url = Uri.parse('$ip/news');
    print('3');
    http.Response response = await http.post(url,
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'X-ACCESS-TOKEN':
token        },
        body: jsonEncode(toJson()));
    print('4');
    int status = response.statusCode;
    print(status);
    notifyListeners();
    final responseData = jsonDecode(response.body);
    String answer = responseData['message'];
    if(status ==200 || status ==201)
    {
      return 'Done Successfully';
    }
    else
    {
      return answer;
    }
  }

  Future editNews(
      {required String title,
      required String newNews,
      required String currentTitle,
      required String token}) async {
    Map<String, dynamic> toJson() => {
          "title": title,
          "content": newNews,
        };

    Uri url = Uri.parse('$ip/news/$currentTitle');
    print('$title + $newNews + $currentTitle');
    http.Response response = await http.put(url,
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'X-ACCESS-TOKEN': token
        },
        body: jsonEncode(toJson()));

    int status = response.statusCode;
    var x = response.body;
    notifyListeners();
    final responseData = jsonDecode(response.body);
    String answer = responseData['message'];
    if(status ==200 || status ==201)
    {
      return 'Done Successfully';
    }
    else
    {
      return answer;
    }
  }

  Future deleteNews(
      {required String currentTitle, required String token}) async {
    print(currentTitle);
    Map<String, dynamic> toJson() => {};
    Uri url = Uri.parse('$ip/news/$currentTitle');
    print('1');
    http.Response response = await http.delete(url,
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'X-ACCESS-TOKEN':
token        },
        body: jsonEncode(toJson()));

    int status = response.statusCode;
    var x = response.body;
    notifyListeners();
    final responseData = jsonDecode(response.body);
    String answer = responseData['message'];
    if(status ==200 || status ==201)
    {
      return 'Done Successfully';
    }
    else
    {
      return answer;
    }
  }
  Future addDriver(
      {required String nationalID,required String name,required int busId,
        required String token}) async
  {
    print('$nationalID + $name + $busId');
    Map<String, dynamic> toJson() => {
      "nationalID": nationalID,
      "name": name,
      "busId": busId
    };
    print('2');
    Uri url = Uri.parse('$ip/users/makeDriver');
    print('3');
    http.Response response = await http.put(url,
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'X-ACCESS-TOKEN':
          token
        },
        body: jsonEncode(toJson()));
    print('4');
    int status = response.statusCode;
    print(status);
    notifyListeners();
    final responseData = jsonDecode(response.body);
    String answer = responseData['message'];
    if(status ==200 || status ==201)
    {
      return 'Done Successfully';
    }
    else
    {
      return answer;
    }
  }
  Future deleteDriver(
      {required var nationalID, required String token}) async {
    Map<String, dynamic> toJson() => {};
    Uri url = Uri.parse('$ip/users/delete-driver/$nationalID');
    print(nationalID);
    http.Response response = await http.put(url,
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'X-ACCESS-TOKEN':
          token        },
        body: jsonEncode(toJson()));

    int status = response.statusCode;
    var x = response.body;
    notifyListeners();
    final responseData = jsonDecode(response.body);
    String answer = responseData['message'];
    if(status ==200 || status ==201)
    {
      return 'Done Successfully';
    }
    else
    {
      return answer;
    }
  }
}
